#include <vector_types.h>
#include <gpu/cuda/cutil_math.h>
/**
 * @brief All the pre-defined colors
 */

// custom colors
static float3 COLOR_RED   = make_float3(1.0f, 0.0f, 0.0f);
static float3 COLOR_GREEN = make_float3(0.0f, 1.0f, 0.0f);
static float3 COLOR_BLUE  = make_float3(0.0f, 0.0f, 1.0f);
static float3 COLOR_YELLOW= make_float3(1.0f, 1.0f, 0.0f);
static float3 COLOR_BLACK = make_float3(0.0f, 0.0f, 0.0f);

// personal preference
static float3 COLOR_BLACK_ZY = make_float3(88.0f/255,   88.0f/255,  88.0f/255 );
static float3 COLOR_PURPLE_ZY= make_float3(102.0f/255,  51.0f/255,  153.0f/255);
