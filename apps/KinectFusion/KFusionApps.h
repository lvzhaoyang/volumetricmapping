#include "kinectfusion.h"
#include "renderFusion.h"
#include "KFusionStream.h"
#include "cudaFusionConfig.h"


/**
 * @brief The KFusionApps class
 *  This will be a KinectFusion application example
 */
class KFusionApps {

public:

  KFusionApps();

  ~KFusionApps();

  /**
   * @brief updateStream
   */
  virtual void updateStream();

  /**
   * @brief run pipelines
   */
  virtual void run();

  /**
   * @brief drawRenderingResult
   *    Use GLUT window to draw 2D rendered result
   */
  virtual void drawRenderingResult();

protected:

  void initBuffers();

  template<typename T, typename A>
  void glDrawWindow(const Image<T,A>& scene,
                    const int x, const int y,
                    const std::string &name);

  KFusion::shared_ptr kfusion_;

  RenderFusion::shared_ptr render_;

  KFusionStream::shared_ptr data_stream_;

  Image<uint16_t, HostDevice> depth_image_[2];
  Image<uchar3, HostDevice> rgb_image_;
  Image<uchar3, HostDevice> depth_model;

  size_t source_index_;

  uint2 image_size_;

  bool to_render_light_;
  bool to_render_track_;
  bool to_render_depth_;
};
