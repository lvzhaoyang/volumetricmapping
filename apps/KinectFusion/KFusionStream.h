#include <string>

#include <vector_types.h>

#include <boost/shared_ptr.hpp>

#include <dataInterface/dataReader.h>
#include <dataInterface/TUM3D.h>

/**
 * @brief The KFusionStream class
 */
class KFusionStream {

public:

  typedef boost::shared_ptr<KFusionStream> shared_ptr;

public:

  KFusionStream(const int stream_type, const size_t source_index);

  ~KFusionStream();

  void initBuffers( uint16_t* depth_temp_buffers[2], uchar3* rgb_buffer );

  bool readOneFrame();

private:

  bool readOneFrameTum3D();

  int stream_type_;

  size_t source_index_;
  size_t frame_num_;

  DataReader::shared_ptr data_reader_ptr_;

  uint16_t* depth_temp_buffers_[2];
  uint16_t* depth_buffer_;
  uchar3* rgb_buffer_;

  /// flag for stream status. Reset to false before reading data
  bool got_depth_;

  static int depth_index_;
};

