#pragma once

#include <gpu/cuda/cudaImage.h>

#include "cudaFusionConfig.h"
#include "kinectfusion.h"

/**
 * @brief The RenderFusion class. Render images for KinectFusion
 */
class RenderFusion {
public:

  typedef boost::shared_ptr<RenderFusion> shared_ptr;

public:
  /**
   * @brief RenderFusion Constructor
   * @param size_x    Image size x
   * @param size_y    Image size y
   */
  RenderFusion(uint size_x, uint size_y);

  /**
   * @brief RenderFusion Desctructor
   */
  ~RenderFusion();

  /**
   * @brief render
   * @param kfusion_ptr
   * @param kfusion_config_ptr
   */
  void render(KFusion::shared_ptr kfusion_ptr,
              KFusionConfig::shared_ptr kfusion_config_ptr);

  /**
   * @brief lightScene
   * @return
   */
  const Image<uchar4, HostDevice>& depthScene() const {
    return depth_scene_;
  }


  /**
   * @brief lightScene
   * @return
   */
  const Image<uchar4, HostDevice>& lightScene() const {
    return light_scene_;
  }

  /**
   * @brief trackScene
   * @return
   */
  const Image<uchar4, HostDevice>& trackScene() const {
    return track_scene_;
  }

  /**
   * @brief textureScene
   * @return
   */
  const Image<uchar4, HostDevice>& textureScene() const {
    return texture_scene_;
  }


private:

  uint2 image_size_;

  bool to_render_depth_;
  bool to_render_light_;
  bool to_render_track_;
  bool to_render_texture_;

  ///
  Image<uchar4, HostDevice> depth_scene_;
  ///
  Image<uchar4, HostDevice> light_scene_;
  ///
  Image<uchar4, HostDevice> track_scene_;
  ///
  Image<uchar4, HostDevice> texture_scene_;
  ///
  Image<float3, Device> vertice_map_;
  ///
  Image<float3, Device> normal_map_;
  ///
  Image<float, Device> depth_map_;

  /// light position
  float3 light_;
  /// ambient charaters
  float3 ambient_;

};
