#include <stdint.h>
#include <iostream>

#include <opencv2/core/core.hpp>

#include "KFusionStream.h"

using namespace std;
using namespace boost;

int KFusionStream::depth_index_ = 0;

/* ************************************************************************* */

KFusionStream::KFusionStream(const int stream_type, const size_t source_index)
  : stream_type_(stream_type), source_index_ (source_index) {

  if(stream_type_ == DataReader::TUM_3D) {
    data_reader_ptr_ = make_shared<Tum3D> ();
    data_reader_ptr_->parseSequenceFile(source_index_);
  } else if(stream_type_ == DataReader::STANFORD_3D) {
    cout << "STANFORD 3D is not implemented yet" << endl;
  } else if(stream_type_ == DataReader::SUN_3D) {
    cout << "SUN 3D is not implemented yet" << endl;
  } else {
    cout << "there is no such stream type" << endl;
  }
}

/* ************************************************************************* */

KFusionStream::~KFusionStream() {

}

/* ************************************************************************* */

void KFusionStream::initBuffers( uint16_t* depth_temp_buffers[2], uchar3* rgb_buffer) {

  depth_temp_buffers_[0] = depth_temp_buffers[0];
  depth_temp_buffers_[1] = depth_temp_buffers[1];

  rgb_buffer_ = rgb_buffer;

  got_depth_ = false;
}

/* ************************************************************************* */

bool KFusionStream::readOneFrame() {

  depth_index_ = (depth_index_ + 1) % 2;

  if(stream_type_ == DataReader::TUM_3D) {
    return readOneFrameTum3D();
  } else {
    return false;
  }
}

/* ************************************************************************* */

bool KFusionStream::readOneFrameTum3D() {
  cv::Mat cvImage, cvDepth;
  int dindex = 0;
  data_reader_ptr_->readDepth(source_index_, frame_num_, cvDepth);
  data_reader_ptr_->readImage(source_index_, frame_num_, cvImage);

  cv::Mat cvImageBGR;
  cv::cvtColor(cvImage, cvImageBGR, CV_RGB2BGR);
  cv::Mat cvImageGray;
  cv::cvtColor(cvImage, cvImageGray, CV_RGB2GRAY);

  if(cvDepth.rows == cvImage.rows && cvDepth.cols == cvImage.cols) {

    // read depth and rgb information together

    for(int row = 0; row < cvDepth.rows; ++row) {
      for(int col = 0; col < cvDepth.cols; ++col) {
        uint16_t c_depth = cvDepth.at<uint16_t>(row, col);
        uchar3 c_rgb = cvImage.at<uchar3>(row, col);
        // the number 5.0 makes me feel uncomfortable.
        *(depth_temp_buffers_[depth_index_] + dindex) = c_depth / 5.0;
        *(rgb_buffer_ + dindex) = c_rgb;
        dindex ++;
      }
    }
  } else {

    // fisrt read in the depth information and then the rgb

    for(int row = 0; row < cvDepth.rows; ++row) {
      for(int col = 0; col < cvDepth.cols; ++col) {
        uint16_t c_depth = cvDepth.at<uint16_t>(row, col);

        *(depth_temp_buffers_[depth_index_] + dindex) = c_depth / 5;
        dindex ++;
      }
    }
    dindex = 0;
    for(int row = 0; row < cvImage.rows; ++row) {
      for(int col = 0; col < cvImage.cols; ++col) {
        uchar3 c_rgb = cvImage.at<uchar3>(row, col);
        *(rgb_buffer_ + dindex) = c_rgb;
        dindex ++;
      }
    }
  }

  got_depth_ = true;
  frame_num_++;

  return true;
}
