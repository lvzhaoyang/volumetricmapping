/* ----------------------------------------------------------------------------

* Volumetric Mapping Libraries in CUDA

* The implementation is based on:
*   https://github.com/GerhardR/kfusion
* Thanks to Gerhard.R. for raw implementation
*
* -------------------------------------------------------------------------- */

/**
  * @file   cudaFusionConfig.h
  * @brief  The configuration file for KinectFusion
  * @author Zhaoyang Lv
  * @date   Sept. 2014
  */

#pragma once

#include <gpu/cuda/cutil_math.h>

#include <cuda_runtime.h>

#include <vector>

#include <boost/shared_ptr.hpp>

/**
 * @brief The KFusionConfig struct. Store all the configurations parameters
 */
struct KFusionConfig {

public:

  typedef boost::shared_ptr<KFusionConfig> shared_ptr;

public:
  /// size of the volume in voxels
  uint3 volumeSize;
  /// real world dimensions spanned by the volume in meters
  float3 volumeDimensions;
  /// the voxel dimension spanned in meters
  float3 voxelDim;

  /// combine tracking and calculating linear system in one
  /// this option saves some time in tracking, but there is no per pixel output anymore
  /// seems there exists an issue here, default to be false
  bool combinedTrackAndReduce;

  /// camera configuration parameters
  float4 camera;
  /// size of the input depth images
  uint2 inputSize;
  /// values for raycasting in meters
  float nearPlane, farPlane;
  /// width of linear ramp, left and right of 0 in meters
  float mu;
  /// maximal weight for volume integration, controls speed of updates
  float maxweight;
  /// bilateral filter radius
  int radius;
  /// gaussian delta
  float delta;
  /// euclidean delta
  float e_delta;
  /// 3D distance threshold for ICP correspondences
  float dist_threshold;
  /// dot product normal threshold for ICP correspondences
  float normal_threshold;
  /// max number of iterations per level
  std::vector<int> iterations;
  /// percent of tracked pixels to accept tracking result
  float track_threshold;
  /// block size for image operations
  dim3 imageBlock;
  /// block size for raycasting
  dim3 raycastBlock;

  /**
   * @brief KFusionConfig Constructor
   */
  KFusionConfig(){
    volumeSize = make_uint3(256);
    volumeDimensions = make_float3(2.f);
    voxelDim = volumeDimensions / make_float3(volumeSize);

    combinedTrackAndReduce = false;

    camera = make_float4(320,320,320,240);
    inputSize = make_uint2(640,480);

    nearPlane = 0.4f;
    farPlane = 4.0f;
    mu = 0.1f;
    maxweight = 100.0f;

    radius = 2;
    delta = 4.0f;
    e_delta = 0.1f;

    dist_threshold = 0.1f;
    normal_threshold = 0.8f;
    iterations.push_back( 10 );
    iterations.push_back( 7 );
    iterations.push_back( 4 );
    track_threshold = 0.15f;

    imageBlock = dim3(32,16);
    raycastBlock = dim3(32,8);
  }

  /**
   * @brief stepSize
   * @return step size for raycasting
   */
  float stepSize() const {
    return min(volumeDimensions)/max(volumeSize);
  }

};
