#include <GL/freeglut.h>

#include <gtsam/base/timing.h>

#include <utilities/colors.h>
#include "KFusionApps.h"

using namespace boost;

/* ************************************************************************* */

KFusionApps::KFusionApps() {

  KFusionConfig config;
  kfusion_ = make_shared<KFusion>(config);

  image_size_ = config.inputSize;

  render_ = make_shared<RenderFusion>(image_size_.x, image_size_.y);

  data_stream_ = make_shared<KFusionStream>(DataReader::TUM_3D, source_index_);

  to_render_light_ = true;
  to_render_track_ = true;
  to_render_depth_ = true;

  source_index_ = 101;
}

/* ************************************************************************* */

KFusionApps::~KFusionApps() {
}

/* ************************************************************************* */

void KFusionApps::updateStream() {
  data_stream_->readOneFrame();
}

/* ************************************************************************* */

void KFusionApps::run() {

  gttic_(ICP_Tracking);
  kfusion_->trackICP();
  gttoc_(ICP_Tracking);

  gttic_(TSDF_Integration);
  kfusion_->integrateTSDF();
  gttoc_(TSDF_Integration);

  gttic_(Ray_Cast);
  kfusion_->rayCast();
  gttoc_(Ray_Cast);

}

/* ************************************************************************* */

void KFusionApps::drawRenderingResult() {

  glClear(GL_COLOR_BUFFER_BIT);

  if(to_render_light_) {
    glDrawWindow(render_->lightScene(),
                 image_size_.x, image_size_.y, "light scene");
  }

  if(to_render_track_) {
    glDrawWindow(render_->trackScene(),
                 image_size_.x, image_size_.y, "track scene");
  }

  if(to_render_depth_) {
    glDrawWindow(render_->depthScene(),
                 image_size_.x, image_size_.y, "depth scene");
  }

}

/* ************************************************************************* */

void KFusionApps::initBuffers() {
  depth_image_[0].alloc(image_size_);
  depth_image_[1].alloc(image_size_);
  rgb_image_.alloc(image_size_);

  // ToDo:
  // move the memset to the image host template
  memset( depth_image_[0].data(), 0, depth_image_[0].size.x * depth_image_[0].size.y * sizeof(uint16_t) );
  memset( depth_image_[1].data(), 0, depth_image_[1].size.x * depth_image_[1].size.y * sizeof(uint16_t) );
  memset( rgb_image_.data(), 0, rgb_image_.size.x * rgb_image_.size.y * sizeof(uchar3));

  uint16_t* depth_buffers[2] = {
    depth_image_[0].data(),
    depth_image_[1].data() };

  data_stream_->initBuffers(depth_buffers, rgb_image_.data());

}

/* ************************************************************************* */

template<typename T, typename A>
void KFusionApps::glDrawWindow(const Image<T,A>& scene,
                               const int x, const int y,
                               const std::string &name) {
  glRasterPos2i(x, y);
  glDrawPixels(scene);
  glPushAttrib(GL_CURRENT_BIT);
  glRasterPos2i(x+10, y+20);
  glColor3fv( (GLfloat*)&COLOR_GREEN );
  glutBitmapString(GLUT_BITMAP_HELVETICA_12,(const unsigned char*)name.c_str() );
}
