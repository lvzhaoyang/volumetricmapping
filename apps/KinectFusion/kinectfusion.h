/* ----------------------------------------------------------------------------

* Volumetric Mapping Libraries in CUDA

* The implementation is based on:
*   https://github.com/GerhardR/kfusion
* Thanks to Gerhard.R. for raw implementation

* -------------------------------------------------------------------------- */

/**
  * @file   kinectfusion.h
  * @brief  Define KinectFusion pipelines
  * @author Zhaoyang Lv
  * @date   Sept. 2014
  */

#pragma once

#include <stdint.h>
#include <iostream>
#include <vector>

#include <cuda_runtime.h>
#include <vector_types.h>
#include <vector_functions.h>

#include <gtsam/geometry/Point3.h>

#include <gpu/cuda/cudaMethods.h>

#include "cudaFusionConfig.h"



// ToDo: depracate this or move them to some other files
// They shouldn't be here

//inline __host__ __device__ void set_default_pose( Matrix4& A)
//{
//  A.data[0].x = 1.0f, A.data[0].y = 0.0f, A.data[0].z = 0.0f, A.data[0].w = 0.0f;
//  A.data[1].x = 0.0f, A.data[1].y = 1.0f, A.data[1].z = 0.0f, A.data[1].w = 0.0f;
//  A.data[2].x = 0.0f, A.data[2].y = 0.0f, A.data[2].z = 1.0f, A.data[2].w = 0.0f;
//  A.data[3].x = 0.0f, A.data[3].y = 0.0f, A.data[3].z = 0.0f, A.data[3].w = 1.0f;
//}

inline Matrix4 getCameraMatrix( const float4 & k ){
  Matrix4 K;
  K.data[0] = make_float4(k.x, 0, k.z, 0);
  K.data[1] = make_float4(0, k.y, k.w, 0);
  K.data[2] = make_float4(0, 0, 1, 0);
  K.data[3] = make_float4(0, 0, 0, 1);
  return K;
}

inline Matrix4 getInverseCameraMatrix( const float4 & k ){
  Matrix4 invK;
  invK.data[0] = make_float4(1.0f/k.x, 0, -k.z/k.x, 0);
  invK.data[1] = make_float4(0, 1.0f/k.y, -k.w/k.y, 0);
  invK.data[2] = make_float4(0, 0, 1, 0);
  invK.data[3] = make_float4(0, 0, 0, 1);
  return invK;
}

/**
 * @brief The KFusion struct
 */
struct KFusion {

public:
  typedef boost::shared_ptr<KFusion> shared_ptr;

public:
  /**
   * @brief KFusion Constructor. Empty so far. Need to call Init function.
   */
  KFusion();

  /**
   * @brief KFusion Constructor. Init parameters w.r.t configurations.
   * @param config      KinectFusion Configurations
   */
  KFusion(const KFusionConfig & config);

  /**
   * @brief Init. Allocates the volume and image data on the device.
   * @param config      KinectFusion Configurations
   */
  void init( const KFusionConfig & config );

  /**
   * @brief Clear. Releases the allocated device memory.
   */
  void clear();

  /**
   * @brief setPose. Sets the current pose of the camera.
   * @param p           Camera Pose.
   */
  void setPose( const Matrix4 & p );

  /**
   * @brief Reset. Reset all reconstruction information.
   */
  void reset();

  /// passes in a metric depth buffer as float array
  template<typename A>
  void setDepth( const Image<float, A> & depth  ){
    rawDepth = depth;
  }

  /**
   * @brief setKinectDeviceDepth.
   *    Passes in depth image in mm in 16-bit unsigned integers reciding on the device.
   * @param in          The input depth image
   */
  void setKinectDeviceDepth( const Image<uint16_t> & in);

  /**
   * @brief Raycast.
   *    Raycast the reference images to track against from the current pose.
   */
  void rayCast();

  /**
   * @brief Track
   *    Estimates new camera position based on the last depth map set and the volume.
   * @return Tracking result status: success or failure.
   */
  bool trackICP();

  /**
   * @brief Integrate.
   *    Integrates the current depth map using the current camera pose.
   */
  void integrateTSDF();

  /**
   * @brief checkDisparity.
   *    Measure the ICP tracking performance
   */
  void checkDisparity();

  /**
   * @brief volume
   * @return tsdf volume
   */
  const Volume& volume() {
    return integration;
  }

  /**
   * @brief currentVertexImage
   * @return reference to current fram vertex image
   */
  const Image<float3, Device>& currentVertexImage() {
    return vertex;
  }

  /**
   * @brief currentNormalImage
   * @return reference to current frame normal image
   */
  const Image<float3, Device>& currentNormalImage() {
    return normal;
  }

  /**
   * @brief historyInputVertexImage
   * @param idx     frame index
   * @return reference to raw input vertex map in history
   */
  const Image<float3, Device>& historyInputVertexImage(size_t idx) {
    assert( idx < inputVertex.size() );
    return inputVertex[idx];
  }

  /**
   * @brief historyInputNormalImage
   * @param idx     frame index
   * @return reference to raw input normal map in history
   */
  const Image<float3, Device>& historyInputNormalImage(size_t idx) {
    assert( idx < inputNormal.size() );
    return inputNormal[idx];
  }

  /**
   * @brief historyInputDepthImage
   * @param idx     frame index
   * @return reference to raw input depth map in history
   */
  const Image<float, Device>& historyInputDepthImage(size_t idx) {
    assert( idx < inputDepth.size() );
    return inputDepth[idx];
  }

  /**
   * @brief trackingStatusImage
   * @return reference to tracking data in 2D image
   */
  const Image<TrackData, Device>& trackingStatusImage() {
    return reduction;
  }

  /**
   * @brief rawDepthImage
   * @return refenrece to raw depth image
   */
  const Image<float, Device>& rawDepthImage() {
    return rawDepth;
  }

private:
  /// TSDF volume
  Volume integration;
  ///
  Image<TrackData, Device> reduction;
  Image<float3, Device> vertex;
  Image<float3, Device> normal;

  std::vector<Image<float3, Device> > inputVertex;
  std::vector<Image<float3, Device> > inputNormal;
  std::vector<Image<float, Device> > inputDepth;

  Image<float, Device> rawDepth;
  Image<float, HostDevice> output;
  Image<float, Device> gaussian;

  KFusionConfig configuration;

  Matrix4 pose, raycastPose;

  /// the incremental pose estimation
  /// To do:
  ///   Replace the pose3 with matrix4
  ///   There shouldn't be a cpu function gtsam pose3 here
  gtsam::Pose3 motion_est;

  /// the number correct matched pixels
  uint pixel_aligned;

};
