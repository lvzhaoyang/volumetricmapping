#include <vector_functions.h>

#include <GL/freeglut.h>

#include <gpu/cuda/cudaRender.h>
#include <utilities/colors.h>
#include "renderFusion.h"

/* ************************************************************************* */

RenderFusion::RenderFusion(uint size_x, uint size_y) {

  image_size_ = make_uint2(size_x, size_y);

  to_render_depth_ = true;
  to_render_light_ = true;
  to_render_track_ = true;
  to_render_texture_ = true;

  light_scene_.alloc(image_size_);
  track_scene_.alloc(image_size_);
  depth_scene_.alloc(image_size_);
  texture_scene_.alloc(image_size_);
  vertice_map_.alloc(image_size_);
  normal_map_.alloc(image_size_);
  depth_map_.alloc(image_size_);

  light_ = make_float3(1, 1, -1.0);
  ambient_ = make_float3(0.1, 0.1, 0.1);
}

/* ************************************************************************* */

RenderFusion::~RenderFusion() {

}

/* ************************************************************************* */

void RenderFusion::render(KFusion::shared_ptr kfusion_ptr,
                          KFusionConfig::shared_ptr kfusion_config_ptr) {
  if(to_render_light_) {
    const Image<float3, Device>& vertex = kfusion_ptr->currentVertexImage();
    const Image<float3, Device>& normal = kfusion_ptr->currentNormalImage();
    renderLight( light_scene_.getDeviceImage(), vertex, normal, light_, ambient_ );
  }

  if(to_render_track_) {
//    renderTrackResult( track_scene_.getDeviceImage(), kfusion_ptr->trackingStatusImage() );
  }

//  if(to_render_depth_) {
//    renderDepthMap( depth_scene_.getDeviceImage(),
//                    kfusion_ptr->rawDepthImage(),
//                    0.1, 100 );
//  }

//  renderInput(vertice_map_, normal_map_, depth_map_,
//              kfusion_ptr->volume(), kfusion_ptr->rawDepthImage())


//  if(to_render_texture_) {

//  }

}
