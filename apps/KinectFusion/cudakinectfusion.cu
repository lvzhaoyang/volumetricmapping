/* ----------------------------------------------------------------------------

* Volumetric Mapping Libraries in CUDA

* The implementation is based on:
*   https://github.com/GerhardR/kfusion
* Thanks to Gerhard.R. for raw implementation
*
* -------------------------------------------------------------------------- */

/**
  * @file   cudakinectfusion.cu
  * @brief  Implementation of KinectFusion pipelines
  * @author Zhaoyang Lv
  * @date   Sept. 2014
  */


#include "kinectfusion.h"

#include <cassert>
#include <stdio.h>

#undef isnan
#undef isfinite

//#define INITIAL_POSE_ 1

#include <iostream>

using namespace std;

//*****************************************************************************************
// initialize 3d voxels with a specefic number indicating occupancy

void writenormxEOFtoFile(int level) {
  FILE* f;
  f = fopen("normx.txt", "a+");
  if(f == NULL) printf("Could not open file !! \n");
  fprintf(f, "%s\t %d\n", "******************************", level);
  fclose(f);
}

/* ************************************************************************* */

template <int HALFSAMPLE>
__global__ void mm2meters( Image<float> depth, const Image<ushort> in ){
  const uint2 pixel = thr2pos2();
  depth[pixel] = in[pixel * (HALFSAMPLE+1)] / 1000.0f;
}

/* ************************************************************************* */

__global__ void calculateDisparity(uint* results, float* acc_error, const Image<TrackData> output) {
  const uint2 pixel = thr2pos2();

  if(pixel.x >= output.size.x || pixel.y >= output.size.y)
    return;

  const TrackData& track_result = output[pixel];

  switch(track_result.result) {
    case -1:
      atomicAdd(results+1, 1); break;
    case -2:
      atomicAdd(results+2, 1); break;
    case -3:
      atomicAdd(results+3, 1); break;
    case -4:
      atomicAdd(results+4, 1); break;
    case -5:
      atomicAdd(results+5, 1); break;
    default:
      atomicAdd(results, 1);
      atomicAdd(acc_error, track_result.error * track_result.error);
      break;
  }
}

/* ************************************************************************* */

KFusion::KFusion() {}

/* ************************************************************************* */


KFusion::KFusion ( const KFusionConfig & config ) {
  init(config);
}

/* ************************************************************************* */

void KFusion::init( const KFusionConfig & config ) {
  configuration = config;

  cudaSetDeviceFlags(cudaDeviceMapHost);

  integration.init(config.volumeSize, config.volumeDimensions);

  reduction.alloc(config.inputSize);
  vertex.alloc(config.inputSize);
  normal.alloc(config.inputSize);
  rawDepth.alloc(config.inputSize);

  inputDepth.resize(config.iterations.size());
  inputVertex.resize(config.iterations.size());
  inputNormal.resize(config.iterations.size());

  for(size_t i = 0; i < config.iterations.size(); ++i){
    inputDepth[i].alloc(config.inputSize >> i);
    inputVertex[i].alloc(config.inputSize >> i);
    inputNormal[i].alloc(config.inputSize >> i);
  }

  gaussian.alloc(make_uint2(config.radius * 2 + 1, 1));
  output.alloc(make_uint2(32,8));

  //generate gaussian array
  generateGaussian<<< 1, gaussian.size.x>>>(gaussian, config.delta, config.radius);
  cudaErrorCheck(cudaGetLastError ());
  cudaErrorCheck(cudaDeviceSynchronize ());

  reset();
}

/* ************************************************************************* */

void KFusion::reset(){
  dim3 block(32,16);
  dim3 grid = divup(dim3(integration.size.x, integration.size.y), block);
  initVolume<<<grid, block>>>(integration, make_float2(1.0f, 0.0f));
  cudaErrorCheck(cudaGetLastError ());
  cudaErrorCheck(cudaDeviceSynchronize ());

}

/* ************************************************************************* */

void KFusion::clear(){
  integration.release();
}

/* ************************************************************************* */

void KFusion::setPose( const Matrix4 & p ){
  pose = p;
}

/* ************************************************************************* */

void KFusion::setKinectDeviceDepth( const Image<uint16_t> & in){
  if(configuration.inputSize.x == in.size.x) {
    mm2meters<0><<<divup(rawDepth.size, configuration.imageBlock), configuration.imageBlock>>>(rawDepth, in);
    cudaErrorCheck(cudaGetLastError ());
    cudaErrorCheck(cudaDeviceSynchronize ());

  } else if(configuration.inputSize.x == in.size.x / 2 ) {
    cudaErrorCheck(cudaGetLastError ());
    cudaErrorCheck(cudaDeviceSynchronize ());

    mm2meters<1><<<divup(rawDepth.size, configuration.imageBlock), configuration.imageBlock>>>(rawDepth, in);
  } else {
    assert(false);
  }
}

/* ************************************************************************* */

void KFusion::rayCast(){
  // raycast integration volume into the depth, vertex, normal buffers
  raycastPose = pose;
  raycast<<<divup(configuration.inputSize, configuration.raycastBlock), configuration.raycastBlock>>>(vertex, normal, integration, raycastPose * getInverseCameraMatrix(configuration.camera), configuration.nearPlane, configuration.farPlane, configuration.stepSize(), 0.50f * configuration.mu);
  cudaErrorCheck(cudaGetLastError ());
  cudaErrorCheck(cudaDeviceSynchronize ());

}

/* ************************************************************************* */

bool KFusion::trackICP() {

  //const Matrix4 invK = getInverseCameraMatrix(configuration.camera);

  vector<dim3> grids;
  for(size_t i = 0, size = configuration.iterations.size(); i < size; ++i)
    grids.push_back(divup(configuration.inputSize >> i, configuration.imageBlock));

  // filter the input depth map
  bilateralFilter<<<grids[0], configuration.imageBlock>>>(inputDepth[0], rawDepth, gaussian, configuration.e_delta, configuration.radius);
  cudaErrorCheck(cudaGetLastError ());
  cudaErrorCheck(cudaDeviceSynchronize ());

  // half sample the input depth maps into the pyramid levels
  for(size_t i = 1, size = configuration.iterations.size(); i < size; ++i) {
    halfSampleRobust<<<grids[i], configuration.imageBlock>>>(inputDepth[i], inputDepth[i-1], configuration.e_delta * 3, 1);
    cudaErrorCheck(cudaGetLastError ());
    cudaErrorCheck(cudaDeviceSynchronize ());

  }

  // prepare the 3D information from the input depth maps
  for(size_t i = 0, size = configuration.iterations.size(); i < size; ++i) {
    depth2vertex<<<grids[i], configuration.imageBlock>>>( inputVertex[i], inputDepth[i], getInverseCameraMatrix(configuration.camera / float(1 << i))); // inverse camera matrix depends on level
    cudaErrorCheck(cudaGetLastError ());
    cudaErrorCheck(cudaDeviceSynchronize ());

    vertex2normal<<<grids[i], configuration.imageBlock>>>( inputNormal[i], inputVertex[i] );
    cudaErrorCheck(cudaGetLastError ());
    cudaErrorCheck(cudaDeviceSynchronize ());

  }

  const Matrix4 oldPose = pose;
  int cpcnt = 1;
  const Matrix4 projectReference = getCameraMatrix(configuration.camera) * inverse(raycastPose);

  //TooN::Matrix<8, 32, float, TooN::Reference::RowMajor> values(output.data());
  solveAndUpdate values( output.data(), pose );

  for(int level = configuration.iterations.size()-1; level >= 0; --level) {
    for(int i = 0; i < configuration.iterations[level]; ++i) {
      if(configuration.combinedTrackAndReduce) {

        trackAndReduce<<<8, 112>>>( output.getDeviceImage().data(), inputVertex[level], inputNormal[level], vertex, normal, pose, projectReference, configuration.dist_threshold, configuration.normal_threshold );
        cudaErrorCheck(cudaGetLastError ());
        cudaErrorCheck(cudaDeviceSynchronize ());

      } else {

        track<<<grids[level], configuration.imageBlock>>>( reduction, inputVertex[level], inputNormal[level], vertex, normal, pose, projectReference, configuration.dist_threshold, configuration.normal_threshold);
        cudaErrorCheck(cudaGetLastError ());
        cudaErrorCheck(cudaDeviceSynchronize ());

        reduce<<<8, 112>>>( output.getDeviceImage().data(), reduction, inputVertex[level].size );             // compute the linear system to solve
        cudaErrorCheck(cudaGetLastError ());
        cudaErrorCheck(cudaDeviceSynchronize ());
      }

      //gtsam::Pose3 motion_est;// = gtsam::Pose3::Expmap(gtsam::Vector(6) << gtsam::Vector3::Constant(0), gtsam::Vector3(0) );
      if( values.solve(motion_est) ) {
        pose = pose3ToMatrix4( motion_est ) * (pose);
      } else {
        //    std::cout << "failure, current pose: " << pose << std::endl;
        break;
      }
      //std::cout << "succeed, current pose: " << pose << std::endl;
      cpcnt++;
    }
  }

  //test on both RSME per pixel and percent of pixels tracked
  printf("RSME Error = %lf \t Track Ratio = %lf \n",  sqrt(values(0,0) / values(0,28)), (values(0,28) / (rawDepth.size.x * rawDepth.size.y)) );
  if((sqrt(values(0,0) / values(0,28)) > 2e-2) || (values(0,28) / (rawDepth.size.x * rawDepth.size.y) < configuration.track_threshold) ) {
    printf("Not updating pose after %d iterations\n", cpcnt);
    pose = oldPose;
    return false;
  }
  printf("Updated Pose correctly \n");
  return true;
}

/* ************************************************************************* */

void KFusion::integrateTSDF() {
  integrate<<<divup(dim3(integration.size.x, integration.size.y), configuration.imageBlock), configuration.imageBlock>>>( integration, rawDepth, inverse(pose), getCameraMatrix(configuration.camera), configuration.mu, configuration.maxweight );
}

/* ************************************************************************* */

void KFusion::checkDisparity()
{
  size_t N = 6;
  uint* d_results;			// the statistics of output pixels in tracking state
  float* d_acc_err;			// the accumulated errors during tracking
  cudaMalloc((void**) & d_results, N*sizeof(uint));
  cudaMalloc((void**) & d_acc_err, sizeof(float));
  cudaMemset(d_results, 0, N*sizeof(uint));
  cudaMemset(d_acc_err, 0, sizeof(float));

  calculateDisparity<<<divup(rawDepth.size, configuration.imageBlock), configuration.imageBlock>>>(d_results, d_acc_err, reduction);
  cudaErrorCheck(cudaGetLastError ());
  cudaErrorCheck(cudaDeviceSynchronize ());

  /// store the tracking information after icp iterations
  uint tracking_results[7];

  float* h_acc_err = new float;
  //uint* h_results = new uint[6];
  cudaMemcpy(&tracking_results, d_results, N*sizeof(uint), cudaMemcpyDeviceToHost);
  cudaMemcpy(h_acc_err, d_acc_err, sizeof(float), cudaMemcpyDeviceToHost);

  pixel_aligned = tracking_results[0];

#if 0 /// this is to print the test results
  std::cout << "the results are:" << std::endl
            << "[1] pixels aligned correctly: \t" << tracking_results[0] << std::endl
            << "[-1] normals in input model is invalid:	" << tracking_results[1] << std::endl
            << "[-2] pixels out of projective image: " << tracking_results[2] << std::endl
            << "[-3] normals in reference model is invalid: " << tracking_results[3] << std::endl
            << "[-4] pixels out of distance threshold: " << tracking_results[4] << std::endl
            << "[-5] pixels in normal threshold: \t" << tracking_results[5] << std::endl
            << "accumulated error is:	\t\t" << *h_acc_err << std::endl;
#endif

  tracking_results[6] = configuration.inputSize.x * configuration.inputSize.y;

  delete h_acc_err;
  cudaFree (d_results);
  cudaFree (d_acc_err);
}


