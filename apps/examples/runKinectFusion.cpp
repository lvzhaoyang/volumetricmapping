/**
 * @file runKinectFusion.cpp
 * @brief basic example of Kfusion class
 * @author Zhaoyang Lv
 * @date Dec. 2014
 */

#include "../KinectFusion/kinectfusion.h"

#include <GL/freeglut.h>

#include <iostream>

#include <boost/smart_ptr/shared_ptr.hpp>

using namespace std;
using namespace boost;

void display() {
  glutSwapBuffers();
}

/* ************************************************************************* */

void idle() {
  glutPostRedisplay();
}

/* ************************************************************************* */

void keys(unsigned char key, int x, int y) {
  switch (key) {
    case 'q':
      exit(0);
      break;
  }
}

/* ************************************************************************* */

void specials(int key, int x, int y) {

}

/* ************************************************************************* */

void reshape(int width, int height) {
  glMatrixMode (GL_MODELVIEW);
  glLoadIdentity();
  glViewport(0, 0, width, height);
  glMatrixMode (GL_PROJECTION);
  glLoadIdentity();
  glColor3f(1.0f, 1.0f, 1.0f);
  glRasterPos2f(-1, 1);
  glOrtho(-0.375, width - 0.375, height - 0.375, -0.375, -1, 1); //offsets to make (0,0) the top left pixel (rather than off the display)
  glPixelZoom(1, -1);
}

/* ************************************************************************* */

void exitFunc() {
  cudaDeviceReset();
}

int main(int argc, char ** argv) {

  KFusionConfig config;
  shared_ptr<KFusion> kfusion( new KFusion(config) );

  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
  glutInitWindowSize(640 * 2 + 640, max(480 * 2, 480) );
  glutCreateWindow("kfusion");
  atexit(exitFunc);
  glutDisplayFunc(display);
  glutKeyboardFunc(keys);
  glutSpecialFunc(specials);
  glutReshapeFunc(reshape);
  glutIdleFunc(idle);

  // start main loop
  glutMainLoop();

  return 0;
}
