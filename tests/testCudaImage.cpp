#include <vector_types.h>

#include <gpu/cuda/cudaImage.h>

#include <CppUnitLite/TestHarness.h>

/* ************************************************************************* */

TEST(testImageMemory, allocation_dellocation) {
  uint2 image_size = make_uint2(640, 480);

  Image<uchar3, Device> device_rgb_image;
  device_rgb_image.alloc(image_size);
  device_rgb_image.free();

  Image<uchar3, Host> host_rgb_image;
  host_rgb_image.alloc(image_size);
  host_rgb_image.free();

  Image<float, Device> device_gray_image(image_size);
  device_gray_image.free();

  Image<float, Host> host_gray_image(image_size);
  host_gray_image.free();

  EXPECT(device_rgb_image.data() == NULL);
  EXPECT(host_rgb_image.data()   == NULL);
  EXPECT(device_gray_image.data()== NULL);
  EXPECT(host_gray_image.data()  == NULL);
}

/* ************************************************************************* */

TEST( testImageCopy, host_device) {
  uint2 image_size = make_uint2(640, 480);

  Image<uchar3, Device> device_rgb_image;
  device_rgb_image.alloc(image_size);

  Image<uchar3, Host> host_rgb_image;
  host_rgb_image.alloc(image_size);

  host_rgb_image = device_rgb_image;

}

/* ************************************************************************* */

TEST( testImageCopy, host_device_reference) {
  uint2 image_size = make_uint2(640, 480);

  Image<uchar3, Device> device_rgb_image;
}

/* ************************************************************************* */

/* ************************************************************************* */
int main() { TestResult tr; return TestRegistry::runAllTests(tr); }
/* ************************************************************************* */
