# README #

Author: Zhaoyang Lv

Still in Construction...

### What is VolumetricMapping? ###

* This repository is for a general purpose mapping library in volumetric methods, for future mapping  
* Current Version 0.1, still in primary construction

### Quick Start ###
In the root library folder execute:

```
#!c++
$ mkdir build
$ cd build
$ cmake ..
$ make check (optional, runs unit tests)
$ make install
```

This repository incorporate different applications based on volumetric mapping. To choose the specific repository, try:

```
#!c++
ccmake ..

```

Change options in CMake table.

### Prerequisite ###
* Boost: >= 1.54 (I installed Boost from the )
* C++ 11 features are enabled in this library. 
* Cuda: >= 6.5 (Test on Cuda arch sm > 3.0, but small functions are incorporated with the SDK examples)
* CMake: > 2.8.9
* dataInterface: a public repository for reading data benchmark (I will add options for on-line device support in near future)
* OpenCV: not necessarily for all. The openCV core libs are used to handle the images. Suggested to be installed. (2.4.9 version).
* PCL: support PCL feature but not necessary.

### Existing Work ###
Now we have 

* Implemented the Kinect Fusion pipeline, based on Richard Gerhard [Kfusion](https://github.com/GerhardR/kfusion). 
* Implemented Tom Whelan's Kintinous pipeline.

Reference:

```
#!bibtex
@inproceedings{Newcombe11ismar,
	author = {Newcombe, Richard A. and Izadi, Shahram and Hilliges, Otmar and Molyneaux, David and Kim, David and Davison, Andrew J. and Kohli, Pushmeet and Shotton, Jamie and Hodges, Steve and Fitzgibbon, Andrew},
	title = {KinectFusion: Real-time Dense Surface Mapping and Tracking},
	booktitle = {Proceedings of the 2011 10th IEEE International Symposium on Mixed and Augmented Reality},
	series = {ISMAR '11},
	year = {2011},
	isbn = {978-1-4577-2183-0},
	pages = {127--136},
	numpages = {10},
	url = {http://dx.doi.org/10.1109/ISMAR.2011.6092378},
	doi = {10.1109/ISMAR.2011.6092378},
	acmid = {2120179},
	publisher = {IEEE Computer Society},
	address = {Washington, DC, USA},
}

@inproceedings{Whelan12rssw,
   author = {T. Whelan and M. Kaess and M.F. Fallon and
	H. Johannsson and J.J. Leonard and J.B. McDonald},
   title = {Kintinuous: Spatially Extended {K}inect{F}usion},
   booktitle = {RSS Workshop on RGB-D: Advanced Reasoning with Depth Cameras},
   address = {Sydney, Australia},
   month = {Jul},
   year = {2012}
}

```