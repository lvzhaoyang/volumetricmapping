/* ----------------------------------------------------------------------------

* Volumetric Mapping Libraries in CUDA

* The implementation is based on:
*   https://github.com/GerhardR/kfusion
* Thanks to Gerhard.R. for raw implementation

* -------------------------------------------------------------------------- */

/**
  * @file   cudaTSDF.cu
  * @brief  Implementation of the TSDF volume data structure for Kinect Fusion
  * @author Zhaoyang Lv
  * @date   Dec. 2014
  */

#include "cudaTSDF.h"

// To Do:
// Integrate color information into the volume

/* ************************************************************************* */

__global__
void integrate( Volume vol,
                const Image<float> depth,
                const Matrix4 invTrack,
                const Matrix4 K,
                const float mu,
                const float maxweight ) {
  uint3 pix = make_uint3(thr2pos2());
  float3 pos = invTrack * vol.pos(pix);
  float3 cameraX = K * pos;
  const float3 delta = rotate(invTrack, make_float3(0,0, vol.dim.z / vol.size.z));
  const float3 cameraDelta = rotate(K, delta);

  for(pix.z = 0; pix.z < vol.size.z; ++pix.z, pos += delta, cameraX += cameraDelta){
    if(pos.z < 0.0001f) // some near plane constraint
      continue;
    const float2 pixel = make_float2(cameraX.x/cameraX.z + 0.5f, cameraX.y/cameraX.z + 0.5f);
    if(pixel.x < 0 || pixel.x > depth.size.x-1 || pixel.y < 0 || pixel.y > depth.size.y-1)
      continue;
    const uint2 px = make_uint2(pixel.x, pixel.y);
    if(depth[px] == 0)
      continue;
    const float diff = (depth[px] - cameraX.z) * sqrt(1+sq(pos.x/pos.z) + sq(pos.y/pos.z));
    if(diff > -mu){
      const float sdf = fminf(1.f, diff/mu);
      float2 data = vol[pix];
      data.x = clamp((data.y*data.x + sdf)/(data.y + 1), -1.f, 1.f);
      data.y = fminf(data.y+1, maxweight);
      vol.set(pix, data);
    }
  }
}

/* ************************************************************************* */

__global__
void initVolume( Volume volume, const float2 val ){
  uint3 pos = make_uint3(thr2pos2());
  for(pos.z = 0; pos.z < volume.size.z; ++pos.z)
    volume.set(pos, val);
}


/* ************************************************************************* */

__global__
void setSphere( Volume volume, const float3 center, const float radius, const float val ) {
  uint3 pos = make_uint3(thr2pos2());
  for(pos.z = 0; pos.z < volume.size.z; ++pos.z) {
      const float d = length(volume.pos(pos) - center);
      if(d < radius)
        volume.set(pos, make_float2(val, 0.0f));
    }
}

/* ************************************************************************* */

__global__
void setBox( Volume volume, const float3 min_corner, const float3 max_corner, const float val ) {
  uint3 pos = make_uint3(thr2pos2());
  for(pos.z = 0; pos.z < volume.size.z; ++pos.z) {
      const float3 p = volume.pos(pos);
      if(min_corner.x < p.x && min_corner.y < p.y && min_corner.z < p.z &&
         p.x < max_corner.x && p.y < max_corner.y && p.z < max_corner.z )
        volume.set(pos, make_float2(val, 0.0f));
    }
}

/* ************************************************************************* */

void initVolumeWrap( Volume volume, const float val ) {
  dim3 block(32,16);
  initVolume<<<divup(dim3(volume.size.x, volume.size.y), block), block>>>(volume, make_float2(val, 0.0f));
}

/* ************************************************************************* */

void setBoxWrap( Volume volume,
                 const float3 min_corner,
                 const float3 max_corner,
                 const float val ) {
  dim3 block(32,16);
  setBox<<<divup(dim3(volume.size.x, volume.size.y), block), block>>>(volume, min_corner, max_corner, val);
}

/* ************************************************************************* */

void setSphereWrap( Volume volume,
                    const float3 center,
                    const float radius,
                    const float val ) {
  dim3 block(32,16);
  setSphere<<<divup(dim3(volume.size.x, volume.size.y), block), block>>>(volume, center, radius, val);
}
