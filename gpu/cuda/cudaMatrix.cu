/* ----------------------------------------------------------------------------

* Volumetric Mapping Libraries in CUDA

* The implementation is based on:
*   https://github.com/GerhardR/kfusion
* Thanks to Gerhard.R. for raw implementation

* -------------------------------------------------------------------------- */

/**
  * @file   cudaMatrix.cu
  * @brief  Matrix methods in cuda
  * @author Zhaoyang Lv
  * @date   Dec. 2014
  */

#include "cudaMatrix.h"

/* ************************************************************************* */

std::ostream & operator<< ( std::ostream & out, const Matrix4 & m ) {

  for(unsigned i = 0; i < 4; ++i)
    out << m.data[i].x << "  "
        << m.data[i].y << "  "
        << m.data[i].z << "  "
        << m.data[i].w << "\n";

  return out;
}

/* ************************************************************************* */

Matrix4 operator* ( const Matrix4 & A, const Matrix4 & B){
  Matrix4 R;

  const float * Af = &(A.data[0].x);
  const float * Bf = &(B.data[0].x);

  Eigen::Matrix<float,4,4> Ae = Eigen::Map<const Eigen::Matrix<float,4,4,Eigen::RowMajor> >(Af);
  Eigen::Matrix<float,4,4> Be = Eigen::Map<const Eigen::Matrix<float,4,4,Eigen::RowMajor> >(Bf);

  Eigen::Matrix<float,4,4> Ce = Eigen::Map<Eigen::Matrix<float,4,4,Eigen::RowMajor> >(&(R.data[0].x));
  Ce = Ae * Be;

  Eigen::Map<Eigen::Matrix<float,4,4,Eigen::RowMajor> > (&(R.data[0].x), 4, 4) = Ce;

  return R;
}

/* ************************************************************************* */

Matrix4 inverse( const Matrix4& A ) {

  const float * Af = &(A.data[0].x);
  Eigen::Matrix<float,4,4> Ae = Eigen::Map<const Eigen::Matrix<float,4,4,Eigen::RowMajor> >(Af);

  Matrix4 R;
  Eigen::Matrix4f IAe = Ae.inverse();

  Eigen::Map<Eigen::Matrix<float,4,4,Eigen::RowMajor> > (&(R.data[0].x), 4, 4) = IAe;

  return R;
}

/* ************************************************************************* */

Matrix4 transpose( const Matrix4 &A ) {

  Matrix4 R;

  R.data[0].x = A.data[0].x;
  R.data[0].y = A.data[1].x;
  R.data[0].z = A.data[2].x;
  R.data[0].w = A.data[3].x;

  R.data[1].x = A.data[0].y;
  R.data[1].y = A.data[1].y;
  R.data[1].z = A.data[2].y;
  R.data[1].w = A.data[3].y;

  R.data[2].x = A.data[0].z;
  R.data[2].y = A.data[1].z;
  R.data[2].z = A.data[2].z;
  R.data[2].w = A.data[3].z;

  R.data[3].x = A.data[0].w;
  R.data[3].y = A.data[1].w;
  R.data[3].z = A.data[2].w;
  R.data[3].w = A.data[3].w;

  return R;
}

/* ************************************************************************* */

bool checkEqual(Matrix4& A, Matrix4& B) {

  for(int i = 0; i < 4; i ++) {
      if( A.data[i].x != B.data[i].x ||
          A.data[i].y != B.data[i].y ||
          A.data[i].z != B.data[i].z ||
          A.data[i].w != B.data[i].w )

        return false;
    }

  return true;
}

/* ************************************************************************* */

Matrix4 pose3ToMatrix4( const gtsam::Pose3 & p) {

  Matrix4 R;
  gtsam::Matrix4 M = p.matrix();
  Eigen::Matrix4f Mf = M.cast<float>(); //cast to float

  Eigen::Map<Eigen::Matrix<float,4,4,Eigen::RowMajor> > (&(R.data[0].x), 4, 4) = Mf;

  return R;
}

/* ************************************************************************* */

gtsam::Pose3 matrix4ToPose3(const Matrix4& poseMatrix4) {

  gtsam::Rot3 rotation (
        poseMatrix4.data[0].x, poseMatrix4.data[0].y, poseMatrix4.data[0].z,
      poseMatrix4.data[1].x, poseMatrix4.data[1].y, poseMatrix4.data[1].z,
      poseMatrix4.data[2].x, poseMatrix4.data[2].z, poseMatrix4.data[2].z );
  gtsam::Point3 translation (
        poseMatrix4.data[0].w, poseMatrix4.data[1].w, poseMatrix4.data[2].w);

  gtsam::Pose3 pose3 (rotation, translation);

  return pose3;
}
