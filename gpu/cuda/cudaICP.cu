/* ----------------------------------------------------------------------------

* Volumetric Mapping Libraries in CUDA

* The implementation is based on:
*   https://github.com/GerhardR/kfusion
* Thanks to Gerhard.R. for raw implementation

* -------------------------------------------------------------------------- */

/**
  * @file   cudaICP.h
  * @brief  Declare Interative-Closest Points methods in cuda
  * @author Zhaoyang Lv
  * @date   Dec. 2014
  */

#include "cutil_math.h"
#include "cudaICP.h"
#include "cudaMatrix.h"
#include "cudaUtility.h"

#define POINT_INVALID -2

/* ************************************************************************* */

Eigen::MatrixXf makeJTJ( const Eigen::Matrix<float,1,21>& v ) {

  Eigen::MatrixXf C;
  C.resize(6,6);
  C = Eigen::Matrix<float,6,6>::Zero();

  C.row(0)          = v.block(0,0,  1,6);
  C.block(1,1, 1,5) = v.block(0,6,  1,5);
  C.block(2,2, 1,4) = v.block(0,11, 1,4);
  C.block(3,3, 1,3) = v.block(0,15, 1,3);
  C.block(4,4, 1,2) = v.block(0,18, 1,2);
  C(5,5) = v(0,20);

  for(int r = 1; r < 6; ++r)
    for(int c = 0; c < r; ++c)
      C(r,c) = C(c,r);

  return C;
}

/* ************************************************************************* */

Eigen::Matrix<float,6,1> solveSVD( const Eigen::Matrix<float,1,27>& vals ){

  const Eigen::Matrix<float,1,6> b = vals.block(0,0,1,6);
  const Eigen::MatrixXf C = makeJTJ(vals.block(0,6,1,21));

  Eigen::Matrix<float,6,1> deltax = C.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(b.transpose());
  return deltax;
}

/* ************************************************************************* */

bool solveAndUpdate::solve(gtsam::Pose3& delta) {
  // Get a 8x32 Matrix
  Eigen::Matrix<float,8,32> values = Eigen::Map<const Eigen::Matrix<float,8,32,Eigen::RowMajor> >(data);

  for(int j = 1; j < 8; ++j)
    values.row(0) += values.row(j);

  Eigen::Matrix<float,6,1> x = solveSVD( values.block(0,1,1,27)); // get the 6x1 state update

  //do an incremental pose
  gtsam::Vector v(6);
  v << (double)x(3,0), (double)x(4,0), (double)x(5,0),(double)x(0,0), (double)x(1,0), (double)x(2,0);

  delta = gtsam::Pose3::Expmap(v);

  //std::cout << "toMatrix4( delta ) : \n" <<  toMatrix4( delta ) << std::endl;
  //writenormxtoFile(x.norm());

  return true;
}

/* ************************************************************************* */

__global__
void track( Image<TrackData> output,
            const Image<float3> inVertex,
            const Image<float3> inNormal,
            const Image<float3> refVertex,
            const Image<float3> refNormal,
            const Matrix4 Ttrack,
            const Matrix4 view,
            const float dist_threshold,
            const float normal_threshold ) {
  const uint2 pixel = thr2pos2();
  if(pixel.x >= inVertex.size.x || pixel.y >= inVertex.size.y )
    return;

  TrackData & row = output[pixel];

  if(inNormal[pixel].x == POINT_INVALID ){
    row.result = -1;
    return;
  }

  const float3 projectedVertex = Ttrack * inVertex[pixel];
  const float3 projectedPos = view * projectedVertex;
  const float2 projPixel = make_float2( projectedPos.x / projectedPos.z + 0.5f, projectedPos.y / projectedPos.z + 0.5f);

  if(projPixel.x < 0 || projPixel.x > refVertex.size.x-1 || projPixel.y < 0 || projPixel.y > refVertex.size.y-1 ){
    row.result = -2;
    return;
  }

  const uint2 refPixel = make_uint2(projPixel.x, projPixel.y);
  const float3 referenceNormal = refNormal[refPixel];

  if(referenceNormal.x == POINT_INVALID){
    row.result = -3;
    return;
  }

  const float3 diff = refVertex[refPixel] - projectedVertex;
  const float3 projectedNormal = rotate(Ttrack, inNormal[pixel]);

  if(length(diff) > dist_threshold ){
    row.result = -4;
    return;
  }
  if(dot(projectedNormal, referenceNormal) < normal_threshold){
    row.result = -5;
    return;
  }

  row.result = 1;
  row.error = dot(referenceNormal, diff);
  ((float3 *)row.J)[0] = referenceNormal;
  ((float3 *)row.J)[1] = cross(projectedVertex, referenceNormal);
}

/* ************************************************************************* */

__global__
void reduce( float * out,
             const Image<TrackData> J,
             const uint2 size ) {
  __shared__ float S[112][32]; // this is for the final accumulation
  const uint sline = threadIdx.x;

  float sums[32];
  float * jtj = sums + 7;
  float * info = sums + 28;

  for(uint i = 0; i < 32; ++i)
    sums[i] = 0;

  for(uint y = blockIdx.x; y < size.y; y += gridDim.x){
    for(uint x = sline; x < size.x; x += blockDim.x ){
      const TrackData & row = J[make_uint2(x, y)];
      if(row.result < 1){
        info[1] += row.result == -4 ? 1 : 0;
        info[2] += row.result == -5 ? 1 : 0;
        info[3] += row.result > -4 ? 1 : 0;
        continue;
      }

      // Error part
      sums[0] += row.error * row.error;

      // JTe part
      for(int i = 0; i < 6; ++i)
        sums[i+1] += row.error * row.J[i];

      // JTJ part, unfortunatly the double loop is not unrolled well...
      jtj[0] += row.J[0] * row.J[0];
      jtj[1] += row.J[0] * row.J[1];
      jtj[2] += row.J[0] * row.J[2];
      jtj[3] += row.J[0] * row.J[3];
      jtj[4] += row.J[0] * row.J[4];
      jtj[5] += row.J[0] * row.J[5];

      jtj[6] += row.J[1] * row.J[1];
      jtj[7] += row.J[1] * row.J[2];
      jtj[8] += row.J[1] * row.J[3];
      jtj[9] += row.J[1] * row.J[4];
      jtj[10] += row.J[1] * row.J[5];

      jtj[11] += row.J[2] * row.J[2];
      jtj[12] += row.J[2] * row.J[3];
      jtj[13] += row.J[2] * row.J[4];
      jtj[14] += row.J[2] * row.J[5];

      jtj[15] += row.J[3] * row.J[3];
      jtj[16] += row.J[3] * row.J[4];
      jtj[17] += row.J[3] * row.J[5];

      jtj[18] += row.J[4] * row.J[4];
      jtj[19] += row.J[4] * row.J[5];

      jtj[20] += row.J[5] * row.J[5];

      // extra info here
      info[0] += 1;
    }
  }

  for(int i = 0; i < 32; ++i) // copy over to shared memory
    S[sline][i] = sums[i];

  __syncthreads();            // wait for everyone to finish

  if(sline < 32){ // sum up columns and copy to global memory in the final 32 threads
    for(unsigned i = 1; i < blockDim.x; ++i)
      S[0][sline] += S[i][sline];
    out[sline+blockIdx.x*32] = S[0][sline];
  }
}

/* ************************************************************************* */

__global__
void trackAndReduce( float * out,
                     const Image<float3> inVertex,
                     const Image<float3> inNormal,
                     const Image<float3> refVertex,
                     const Image<float3> refNormal,
                     const Matrix4 Ttrack,
                     const Matrix4 view,
                     const float dist_threshold,
                     const float normal_threshold ) {
  __shared__ float S[112][32]; // this is for the final accumulation
  const uint sline = threadIdx.x;

  float sums[32];
  float * jtj = sums + 7;
  float * info = sums + 28;

  for(uint i = 0; i < 32; ++i)
    sums[i] = 0;

  float J[6];

  for(uint y = blockIdx.x; y < inVertex.size.y; y += gridDim.x){
    for(uint x = sline; x < inVertex.size.x; x += blockDim.x ){
      const uint2 pixel = make_uint2(x,y);

      if(inNormal[pixel].x == POINT_INVALID){
        continue;
      }

      const float3 projectedVertex = Ttrack * inVertex[pixel];
      const float3 projectedPos = view * projectedVertex;
      const float2 projPixel = make_float2( projectedPos.x / projectedPos.z + 0.5f, projectedPos.y / projectedPos.z + 0.5f);

      if(projPixel.x < 0 || projPixel.x > refVertex.size.x-1 || projPixel.y < 0 || projPixel.y > refVertex.size.y-1 ){
        info[3] += 1;
        continue;
      }

      const uint2 refPixel = make_uint2(projPixel.x, projPixel.y);

      if(refNormal[refPixel].x == POINT_INVALID){
        info[3] += 1;
        continue;
      }

      const float3 referenceNormal = refNormal[refPixel];
      const float3 diff = refVertex[refPixel] - projectedVertex;
      const float3 projectedNormal = rotate(Ttrack, inNormal[pixel]);

      if(length(diff) > dist_threshold ){
        info[1] += 1;
        continue;
      }
      if(dot(projectedNormal, referenceNormal) < normal_threshold){
        info[2] += 1;
        continue;
      }

      const float error = dot(referenceNormal, diff);
      ((float3 *)J)[0] = referenceNormal;
      ((float3 *)J)[1] = cross(projectedVertex, referenceNormal);

      // Error part
      sums[0] += error * error;

      // JTe part
      for(int i = 0; i < 6; ++i)
        sums[i+1] += error * J[i];

      // JTJ part
      jtj[0] += J[0] * J[0];
      jtj[1] += J[0] * J[1];
      jtj[2] += J[0] * J[2];
      jtj[3] += J[0] * J[3];
      jtj[4] += J[0] * J[4];
      jtj[5] += J[0] * J[5];

      jtj[6] += J[1] * J[1];
      jtj[7] += J[1] * J[2];
      jtj[8] += J[1] * J[3];
      jtj[9] += J[1] * J[4];
      jtj[10] += J[1] * J[5];

      jtj[11] += J[2] * J[2];
      jtj[12] += J[2] * J[3];
      jtj[13] += J[2] * J[4];
      jtj[14] += J[2] * J[5];

      jtj[15] += J[3] * J[3];
      jtj[16] += J[3] * J[4];
      jtj[17] += J[3] * J[5];

      jtj[18] += J[4] * J[4];
      jtj[19] += J[4] * J[5];

      jtj[20] += J[5] * J[5];

      // extra info here
      info[0] += 1;
    }
  }

  for(int i = 0; i < 32; ++i) // copy over to shared memory
    S[sline][i] = sums[i];

  __syncthreads();            // wait for everyone to finish

  if(sline < 32){ // sum up columns and copy to global memory in the final 32 threads
    for(unsigned i = 1; i < blockDim.x; ++i)
      S[0][sline] += S[i][sline];
    out[sline+blockIdx.x*32] = S[0][sline];
  }
}
