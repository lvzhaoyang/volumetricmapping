/* ----------------------------------------------------------------------------

* Volumetric Mapping Libraries in CUDA

* The implementation is based on:
*   https://github.com/GerhardR/kfusion
* Thanks to Gerhard.R. for raw implementation
*
* -------------------------------------------------------------------------- */

/**
  * @file   cudaMap.h
  * @brief  Implementation of mapping functions in Cuda.
  * @author Zhaoyang Lv
  * @date   Dec. 2014
  */


#include "cudaImage.h"
#include "cudaMatrix.h"

/**
 * @brief depth2vertex Compute vertex image from depth image.
 * @param vertex      the output vertex map
 * @param depth       the intput depth map
 * @param invK        the inverse matrix of camera intrinsic parameters
 */
__global__
void depth2vertex( Image<float3> vertex,
                   const Image<float> depth,
                   const Matrix4 invK );

/**
 * @brief vertex2normal Compute normal image from vertex image
 * @param normal      the output normal map
 * @param vertex      the input vertex map
 */
__global__
void vertex2normal( Image<float3> normal,
                    const Image<float3> vertex );
