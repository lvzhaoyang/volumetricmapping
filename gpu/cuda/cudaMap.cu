/* ----------------------------------------------------------------------------

* Volumetric Mapping Libraries in CUDA

* The implementation is based on:
*   https://github.com/GerhardR/kfusion
* Thanks to Gerhard.R. for raw implementation
*
* -------------------------------------------------------------------------- */

/**
  * @file   cudaMap.cu
  * @brief  Implementation of mapping functions in Cuda.
  * @author Zhaoyang Lv
  * @date   Dec. 2014
  */

#include "cudaMap.h"

#define INVALID -2   // this is used to mark invalid entries in normal or vertex maps

/* ************************************************************************* */

__global__
void depth2vertex( Image<float3> vertex,
                   const Image<float> depth,
                   const Matrix4 invK ) {
  const uint2 pixel = thr2pos2();
  if(pixel.x >= depth.size.x || pixel.y >= depth.size.y )
    return;

  if(depth[pixel] > 0) {
    vertex[pixel] = depth[pixel] * (rotate(invK, make_float3(pixel.x, pixel.y, 1.f)));
  } else {
    vertex[pixel] = make_float3(0);
  }
}

__global__
void vertex2normal( Image<float3> normal,
                    const Image<float3> vertex ) {
  const uint2 pixel = thr2pos2();
  if(pixel.x >= vertex.size.x || pixel.y >= vertex.size.y )
    return;

  const float3 left = vertex[make_uint2(max(int(pixel.x)-1,0), pixel.y)];
  const float3 right = vertex[make_uint2(min(pixel.x+1,vertex.size.x-1), pixel.y)];
  const float3 up = vertex[make_uint2(pixel.x, max(int(pixel.y)-1,0))];
  const float3 down = vertex[make_uint2(pixel.x, min(pixel.y+1,vertex.size.y-1))];

  if(left.z == 0 || right.z == 0 || up.z == 0 || down.z == 0) {
    normal[pixel].x = INVALID;
    return;
  }

  const float3 dxv = right - left;
  const float3 dyv = down - up;
  normal[pixel] = normalize(cross(dyv, dxv)); // switched dx and dy to get factor -1
}
