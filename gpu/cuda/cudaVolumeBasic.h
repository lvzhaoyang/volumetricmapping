
#include <vector_types.h>
#include <thrust/device_vector.h>

/**
 * @brief VolumeBasic
 */
template<typename T>
class VolumeBasic {

public:

  /**
   * @brief VolumeBasic Constructor
   */
  VolumeBasic() {
    size_ = make_uint3(0);
    dim_ = make_float3(1);

    data_.reserve(size_.x * size_.y * size_.z);
  }

  /**
   * @brief VolumeBasic Constructor
   * @param size  volume size
   * @param dim   volume dimension
   */
  VolumeBasic(uint3 size, float3 dim) {
    size_ = size;
    dim_ = dim;

    data_.reserve(size_.x * size_.y * size_.z);
  }

  ~VolumeBasic() {

  }

  void init(uint3 size, float3 dim) {
    size_ = size;
    dim_ = dim;
  }

  void release() {
    data_.clear();
  }

private:
  /// volume size
  uint3 size_;
  /// volume dimension
  float3 dim_;
  /// volume data
  thrust::device_vector<T> data_;
};
