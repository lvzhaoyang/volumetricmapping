/* ----------------------------------------------------------------------------

* Volumetric Mapping Libraries in CUDA

* The implementation is based on:
*   https://github.com/GerhardR/kfusion
* Thanks to Gerhard.R. for raw implementation

* -------------------------------------------------------------------------- */

/**
  * @file   cudaRender.h
  * @brief  Render methods in cuda
  * @author Zhaoyang Lv
  * @date   Dec. 2014
  */

#pragma once

#include <cuda_gl_interop.h> // includes cuda_gl_interop.h

#include "cudaImage.h"
#include "cudaMatrix.h"
#include "cudaICP.h"
#include "cudaTSDF.h"

#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#undef min
#undef max
#endif

/**
 * @brief renderDepthMap. Scales the depth map from near-far to 0-1
 * @param out         The output image
 * @param nearPlane   The near plane depth
 * @param farPlane    The far plane depth
 */
void renderDepthMap( Image<uchar3> out,
                     const Image<float> &,
                     const float nearPlane,
                     const float farPlane );

/**
 * @brief renderNormalMap. Renders normals into a RGB normalmap
 * @param out         The output image
 * @param normal      The input normal map
 */
void renderNormalMap( Image<uchar3> out,
                      const Image<float3> & normal);

/**
 * @brief renderLight. Renders into a grayscale intensity map with lightsource
 * @param out         Grayscale intensity map
 * @param vertex      Vertex map
 * @param normal      Normal map
 * @param light       lightsource position
 * @param ambient     ambient lighting
 */
void renderLight( Image<uchar4> out,
                  const Image<float3, Device> & vertex,
                  const Image<float3, Device> & normal,
                  const float3 & light,
                  const float3 & ambient );

/**
 * @brief renderTrackResult.
 * @param out
 * @param data
 */
void renderTrackResult( Image<uchar4> out,
                        const Image<TrackData> & data );

/**
 * @brief renderVolumeLight
 * @param out
 * @param volume
 * @param view
 * @param nearPlane
 * @param farPlane
 * @param largestep
 * @param light
 * @param ambient
 */
void renderVolumeLight( Image<uchar4> out,
                        const Volume & volume,
                        const Matrix4 view,
                        const float nearPlane,
                        const float farPlane,
                        const float largestep,
                        const float3 light,
                        const float3 ambient );

/**
 * @brief renderInput
 * @param pos3D
 * @param normal
 * @param depth
 * @param volume
 * @param view
 * @param nearPlane
 * @param farPlane
 * @param step
 * @param largestep
 */
void renderInput( Image<float3> pos3D,
                  Image<float3> normal,
                  Image<float> depth,
                  const Volume volume,
                  const Matrix4 view,
                  const float nearPlane,
                  const float farPlane,
                  const float step,
                  const float largestep );

/**
 * @brief renderTexture
 * @param out
 * @param vertex
 * @param normal
 * @param texture
 * @param texproj
 * @param light
 */
void renderTexture( Image<uchar4> out,
                    const Image<float3> & vertex,
                    const Image<float3> & normal,
                    const Image<uchar3> & texture,
                    const Matrix4 & texproj,
                    const float3 light );

/**
 * @brief renderFusedMap
 * @param out
 * @param vertex
 */
void renderFusedMap( Image<uint16_t> out,
                     const Image<float3> & vertex);


template <typename T> struct gl;

template<> struct gl<unsigned char> {
  static const int format=GL_LUMINANCE;
  static const int type  =GL_UNSIGNED_BYTE;
};

template<> struct gl<uchar3> {
  static const int format=GL_RGB;
  static const int type  =GL_UNSIGNED_BYTE;
};

template<> struct gl<uchar4> {
  static const int format=GL_RGBA;
  static const int type  =GL_UNSIGNED_BYTE;
};

template<> struct gl<float> {
  static const int format=GL_LUMINANCE;
  static const int type  =GL_FLOAT;
};

template<> struct gl<float3> {
  static const int format=GL_RGB;
  static const int type  =GL_FLOAT;
};

template<> struct gl<uint16_t> {
  static const int format=GL_LUMINANCE;
  static const int type  =GL_UNSIGNED_SHORT;
};

template <typename T, typename A>
inline void glDrawPixels( const Image<T, A> & i ){
  ::glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  ::glPixelStorei(GL_UNPACK_ROW_LENGTH, i.size.x);
  ::glDrawPixels(i.size.x, i.size.y, gl<T>::format, gl<T>::type, i.data());
}
