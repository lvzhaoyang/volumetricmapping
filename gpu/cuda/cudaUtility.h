/* ----------------------------------------------------------------------------

* Volumetric Mapping Libraries in CUDA

* The implementation is based on:
*   https://github.com/GerhardR/kfusion
* Thanks to Gerhard.R. for raw implementation

* -------------------------------------------------------------------------- */

/**
 * @file   cudaUtility.h
 * @brief  Provide gpus tools to check device status
 * @author Zhaoyang Lv
 * @date   Dec. 2014
 */

#pragma once


#include <stdio.h>
#include <assert.h>
#include <iostream>
#include <cuda_runtime.h>



#define cudaErrorCheck(ans) { gpuAssert((ans), __FILE__, __LINE__); }

/* ************************************************************************* */

inline int printCUDAError() {
  cudaError_t error = cudaGetLastError();
  if(error)
    std::cout << cudaGetErrorString(error) << std::endl;
  return error;
}

/* ************************************************************************* */

inline void gpuAssert(cudaError_t code, std::string file, int line, bool abort=true) {
  if (code != cudaSuccess) {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file.c_str(), line);
      if (abort) exit(code);
    }
}

/* ************************************************************************* */

inline void showGPUMemory() {
  size_t f, t;
  cudaSetDevice(0);
  cudaMemGetInfo(&f, &t);
  std::cout << "the free global GPU memory before display is: " << (f >> 20) << "MB" << std::endl;
  std::cout << "the total memory is: " << (t >> 20) << "MB" << std::endl;
}

/* ************************************************************************* */

inline __device__ uint2 thr2pos2(){
#ifdef __CUDACC__
  return make_uint2( __umul24(blockDim.x, blockIdx.x) + threadIdx.x,
                     __umul24(blockDim.y, blockIdx.y) + threadIdx.y);
#else
  return make_uint2(0, 0);
#endif

}

/* ************************************************************************* */

inline int divup(int a, int b) {
    return (a % b != 0) ? (a / b + 1) : (a / b);
}

/* ************************************************************************* */

inline dim3 divup( uint2 a, dim3 b) {
    return dim3(divup(a.x, b.x), divup(a.y, b.y));
}

/* ************************************************************************* */

inline dim3 divup( dim3 a, dim3 b) {
    return dim3(divup(a.x, b.x), divup(a.y, b.y), divup(a.z, b.z));
}

