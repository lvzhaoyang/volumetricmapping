/* ----------------------------------------------------------------------------

* Volumetric Mapping Libraries in CUDA

* The implementation is based on:
*   https://github.com/GerhardR/kfusion
* Thanks to Gerhard.R. for raw implementation

* -------------------------------------------------------------------------- */

/**
  * @file   cudaImage.h
  * @brief  Image templates in cuda
  * @author Zhaoyang Lv
  * @date   Dec. 2014
  */

#pragma once

#include <cuda_runtime.h>

#include "cutil_math.h"
#include "cudaUtility.h"

/**
  * @brief The Ref struct
  */
struct Ref {

  Ref( void * d = NULL) :
    data(d) {
  }

  void * data;
};

/**
 * @brief The Host struct
 */
struct Host {
  /**
   * @brief Host Constructor
   */
  Host() :
    data(NULL) {
  }

  /**
   * @brief Host destructor
   */
  ~Host() {
  }

  /**
   * @brief alloc. Allocate memory resources in host
   * @param size    Memory Size.
   */
  void alloc( uint size ) {
    cudaHostAlloc( &data, size, cudaHostAllocDefault);
  }

  /**
   * @brief free. Free memory resources in host
   */
  void free() {
    if(data != NULL) {
      cudaFreeHost(data);
      data = NULL;
    }
  }

  void * data;
};


/**
 * @brief The Device struct
 */
struct Device {
  /**
   * @brief Device Constructor
   */
  Device() :
    data(NULL) {
  }

  /**
   * @brief Device Destructor
   */
  ~Device() {
  }

  /**
   * @brief alloc. Allocate memory resources in device
   * @param size    Memory Size
   */
  void alloc( uint size ) {
    cudaMalloc( &data, size );
    cudaMemset( &data, 0, size);
  }

  /**
   * @brief free. Free memory resources in device
   */
  void free() {
    if(data != NULL) {
      cudaFree(data);
      data = NULL;
    }
  }

  void * data;

};

// beware of implicit freeing of memory
struct HostDevice {

  HostDevice()
    : data(NULL) {
  }

  ~HostDevice() {
  }

  void * data;

  void alloc( uint size ) {
    cudaHostAlloc( &data, size, cudaHostAllocMapped );
  }

  void * getDevice() const {
    void * devicePtr;
    cudaHostGetDevicePointer(&devicePtr, data, 0);
    return devicePtr;
  }

  void free() {
    if(data != NULL) {
      cudaFreeHost(data);
      data = NULL;
    }
  }
};

//*********************************************************************

//Overloaded Image Copy operations
//*********************************************************************
template <typename OTHER>
inline void image_copy( Ref & to, const OTHER & from, uint size ){
  to.data = from.data;
}

inline void image_copy( Host & to, const Host & from, uint size ){
  cudaMemcpy(to.data, from.data, size, cudaMemcpyHostToHost);
}

inline void image_copy( Host & to, const Device & from, uint size ){
  cudaMemcpy(to.data, from.data, size, cudaMemcpyDeviceToHost);
}

inline void image_copy( Host & to, const HostDevice & from, uint size ){
  cudaMemcpy(to.data, from.data, size, cudaMemcpyHostToHost);
}

inline void image_copy( Device & to, const Ref & from, uint size ){
  cudaMemcpy(to.data, from.data, size, cudaMemcpyDeviceToDevice);
}

inline void image_copy( Device & to, const Host & from, uint size ){
  cudaMemcpy(to.data, from.data, size, cudaMemcpyHostToDevice);
}

inline void image_copy( Device & to, const Device & from, uint size ){
  cudaMemcpy(to.data, from.data, size, cudaMemcpyDeviceToDevice);
}

inline void image_copy( Device & to, const HostDevice & from, uint size ){
  cudaMemcpy(to.data, from.getDevice(), size, cudaMemcpyDeviceToDevice);
}

inline void image_copy( HostDevice & to, const Host & from, uint size ){
  cudaMemcpy(to.data, from.data, size, cudaMemcpyHostToHost);
}

inline void image_copy( HostDevice & to, const Device & from, uint size ){
  cudaMemcpy(to.getDevice(), from.data, size, cudaMemcpyDeviceToDevice);
}

inline void image_copy( HostDevice & to, const HostDevice & from, uint size ){
  cudaMemcpy(to.data, from.data, size, cudaMemcpyHostToHost);
}

//*********************************************************************

//default of image structure to ref, 2 template scenario
//*********************************************************************
template <typename T, typename Allocator = Ref>
struct Image : public Allocator {

  typedef T PIXEL_TYPE;
  uint2 size;

  Image()
    : Allocator() {
    size = make_uint2(0);
  }

  Image( const uint2 & s ) {
    alloc(s);
  }

  void alloc( const uint2 & s ){
    if(s.x == size.x && s.y == size.y)
      return;
    Allocator::alloc( s.x * s.y * sizeof(T) );
    size = s;
  }

  __device__ T & el(){
    return operator[](thr2pos2());
  }

  __device__ const T & el() const {
    return operator[](thr2pos2());
  }

  __device__ T & operator[](const uint2 & pos ){
    return static_cast<T *>(Allocator::data)[pos.x + size.x * pos.y];
  }

  __device__ const T & operator[](const uint2 & pos ) const {
    return static_cast<const T *>(Allocator::data)[pos.x + size.x * pos.y];
  }

  Image<T> getDeviceImage() {
    return Image<T>(size, Allocator::getDevice());
  }

  operator Image<T>() {
    return Image<T>(size, Allocator::data);
  }

  template <typename A1>
  Image<T, Allocator> & operator=( const Image<T, A1> & other ){
    image_copy(*this, other, size.x * size.y * sizeof(T));
    return *this;
  }

  T * data() {
    return static_cast<T *>(Allocator::data);
  }

  const T * data() const {
    return static_cast<const T *>(Allocator::data);
  }

};

//*********************************************************************

//held to ref, single template format
//*********************************************************************
template <typename T>
struct Image<T, Ref> : public Ref {
  typedef T PIXEL_TYPE;
  uint2 size;

  Image() {
    size = make_uint2(0,0);
  }

  Image( const uint2 & s, void * d )
    : Ref(d), size(s) {
  }

  __device__ T & el(){
    return operator[](thr2pos2());
  }

  __device__ const T & el() const {
    return operator[](thr2pos2());
  }

  __device__ T & operator[](const uint2 & pos ){
    return static_cast<T *>(Ref::data)[pos.x + size.x * pos.y];
  }

  __device__ const T & operator[](const uint2 & pos ) const {
    return static_cast<const T *>(Ref::data)[pos.x + size.x * pos.y];
  }

  T * data() {
    return static_cast<T *>(Ref::data);
  }

  const T * data() const {
    return static_cast<const T *>(Ref::data);
  }
};

