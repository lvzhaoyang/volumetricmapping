/* ----------------------------------------------------------------------------

* Volumetric Mapping Libraries in CUDA

* The implementation is based on:
*   https://github.com/GerhardR/kfusion
* Thanks to Gerhard.R. for raw implementation

* -------------------------------------------------------------------------- */

/**
  * @file   gpuTSDF.h
  * @brief  The TSDF volume data structure for Kinect Fusion
  * @author Zhaoyang Lv
  */

#pragma once

#include <cuda_runtime.h>

#include "cudaImage.h"
#include "cudaMatrix.h"

#include "cutil_math.h"

/**
 * Data structure for The TSDF volume
 */
struct Volume {
  uint3 size;
  float3 dim;

  /// the tsdf value and weight for each voxel;
  short2 * data;
  /// the color for each voxel;
  uchar3 * color;

  /**
   * @brief Volume Constructor
   */
  Volume() {
    size = make_uint3(0);
    dim  = make_float3(1);
    data = NULL;
    color= NULL;
  }

  /**
   * @brief operator[]. Get the tsdf value and tsdf weight.
   * @param pos       Local voxel position in TSDF volume
   *
   * @return The TSDF value pair.
   */
  __device__ __forceinline__
  float2 operator[]( const uint3 & pos ) const {
    const short2 d = data[pos.x + pos.y * size.x + pos.z * size.x * size.y];
    return make_float2(d.x * 0.00003051944088f, d.y); //  / 32766.0f
  }

  /**
   * @brief get_sdf. Get the tsdf value of voxel
   * @param pos       Local voxel position in TSDF volume
   *
   * @return The TSDF value [-1, 1]
   */
  __device__ __forceinline__
  float get_sdf(const uint3 & pos) const {
    return data[pos.x + pos.y * size.x + pos.z * size.x * size.y].x;
  }

  /**
   * @brief get_color. Get the color of voxel
   * @param pos       Local voxel position in TSDF volume
   *
   * @return  Color in uchar3
   */
  __device__ __forceinline__
  uchar3 get_color(const uint3 & pos) const {
    return color[pos.x + pos.y * size.x + pos.z * size.x * size.y];
  }

  /**
   * @brief set. Set the tsdf value and tsdf weight of one voxel
   * @param pos       Local voxel position in TSDF volume
   * @param d         The corresponding TSDF value pair
   */
  __device__ __forceinline__
  void set(const uint3 & pos, const float2 & d ){
    data[pos.x + pos.y * size.x + pos.z * size.x * size.y] = make_short2(d.x * 32766.0f, d.y);
  }

  /**
   * @brief set_color. Set the color of one voxel
   * @param pos       Local voxel position in TSDF volume
   * @param c         The corresponding voxel color
   */
  __device__ __forceinline__
  void set_color(const uint3 & pos, const uchar3 & c) {
    color[pos.x + pos.y * size.x + pos.z * size.x * size.y] = c;
  }

  /**
   * @brief pos. Get the local 3D position of voxel in centimeters
   * @param p         Local voxel position in TSDF volume
   *
   * @return Position in centimeters
   */
  __device__ __forceinline__
  float3 pos( const uint3 & p ) const {
    return make_float3((p.x + 0.5f) * dim.x / size.x, (p.y + 0.5f) * dim.y / size.y, (p.z + 0.5f) * dim.z / size.z);
  }

  /**
   * @brief interp_sdf. Get value from interpolation
   * @param pos       Local voxel position in TSDF volume
   *
   * @return Interpolated sdf value
   */
  __device__ float interp_sdf( const float3 & pos ) const {
    const float3 scaled_pos = make_float3((pos.x * size.x / dim.x) - 0.5f, (pos.y * size.y / dim.y) - 0.5f, (pos.z * size.z / dim.z) - 0.5f);
    const int3 base = make_int3(floorf(scaled_pos));
    const float3 factor = fracf(scaled_pos);
    const int3 lower = max(base, make_int3(0));
    const int3 upper = min(base + make_int3(1), make_int3(size) - make_int3(1));
    return (
          ((get_sdf(make_uint3(lower.x, lower.y, lower.z)) * (1-factor.x) + get_sdf(make_uint3(upper.x, lower.y, lower.z)) * factor.x) * (1-factor.y)
           + (get_sdf(make_uint3(lower.x, upper.y, lower.z)) * (1-factor.x) + get_sdf(make_uint3(upper.x, upper.y, lower.z)) * factor.x) * factor.y) * (1-factor.z)
          + ((get_sdf(make_uint3(lower.x, lower.y, upper.z)) * (1-factor.x) + get_sdf(make_uint3(upper.x, lower.y, upper.z)) * factor.x) * (1-factor.y)
             + (get_sdf(make_uint3(lower.x, upper.y, upper.z)) * (1-factor.x) + get_sdf(make_uint3(upper.x, upper.y, upper.z)) * factor.x) * factor.y) * factor.z
          ) * 0.00003051944088f;
  }

  /**
   * @brief grad. Get gradient value at some position.
   * @param pos         Local voxel position in TSDF volume
   *
   * @return Gradient value
   */
  __device__ float3 grad( const float3 & pos ) const {
    const float3 scaled_pos = make_float3((pos.x * size.x / dim.x) - 0.5f, (pos.y * size.y / dim.y) - 0.5f, (pos.z * size.z / dim.z) - 0.5f);
    const int3 base = make_int3(floorf(scaled_pos));
    const float3 factor = fracf(scaled_pos);
    const int3 lower_lower = max(base - make_int3(1), make_int3(0));
    const int3 lower_upper = max(base, make_int3(0));
    const int3 upper_lower = min(base + make_int3(1), make_int3(size) - make_int3(1));
    const int3 upper_upper = min(base + make_int3(2), make_int3(size) - make_int3(1));
    const int3 & lower = lower_upper;
    const int3 & upper = upper_lower;

    float3 gradient;

    gradient.x =
        (((get_sdf(make_uint3(upper_lower.x, lower.y, lower.z)) - get_sdf(make_uint3(lower_lower.x, lower.y, lower.z))) * (1-factor.x)
          + (get_sdf(make_uint3(upper_upper.x, lower.y, lower.z)) - get_sdf(make_uint3(lower_upper.x, lower.y, lower.z))) * factor.x) * (1-factor.y)
         + ((get_sdf(make_uint3(upper_lower.x, upper.y, lower.z)) - get_sdf(make_uint3(lower_lower.x, upper.y, lower.z))) * (1-factor.x)
            + (get_sdf(make_uint3(upper_upper.x, upper.y, lower.z)) - get_sdf(make_uint3(lower_upper.x, upper.y, lower.z))) * factor.x) * factor.y) * (1-factor.z)
        + (((get_sdf(make_uint3(upper_lower.x, lower.y, upper.z)) - get_sdf(make_uint3(lower_lower.x, lower.y, upper.z))) * (1-factor.x)
            + (get_sdf(make_uint3(upper_upper.x, lower.y, upper.z)) - get_sdf(make_uint3(lower_upper.x, lower.y, upper.z))) * factor.x) * (1-factor.y)
           + ((get_sdf(make_uint3(upper_lower.x, upper.y, upper.z)) - get_sdf(make_uint3(lower_lower.x, upper.y, upper.z))) * (1-factor.x)
              + (get_sdf(make_uint3(upper_upper.x, upper.y, upper.z)) - get_sdf(make_uint3(lower_upper.x, upper.y, upper.z))) * factor.x) * factor.y) * factor.z;

    gradient.y =
        (((get_sdf(make_uint3(lower.x, upper_lower.y, lower.z)) - get_sdf(make_uint3(lower.x, lower_lower.y, lower.z))) * (1-factor.x)
          + (get_sdf(make_uint3(upper.x, upper_lower.y, lower.z)) - get_sdf(make_uint3(upper.x, lower_lower.y, lower.z))) * factor.x) * (1-factor.y)
         + ((get_sdf(make_uint3(lower.x, upper_upper.y, lower.z)) - get_sdf(make_uint3(lower.x, lower_upper.y, lower.z))) * (1-factor.x)
            + (get_sdf(make_uint3(upper.x, upper_upper.y, lower.z)) - get_sdf(make_uint3(upper.x, lower_upper.y, lower.z))) * factor.x) * factor.y) * (1-factor.z)
        + (((get_sdf(make_uint3(lower.x, upper_lower.y, upper.z)) - get_sdf(make_uint3(lower.x, lower_lower.y, upper.z))) * (1-factor.x)
            + (get_sdf(make_uint3(upper.x, upper_lower.y, upper.z)) - get_sdf(make_uint3(upper.x, lower_lower.y, upper.z))) * factor.x) * (1-factor.y)
           + ((get_sdf(make_uint3(lower.x, upper_upper.y, upper.z)) - get_sdf(make_uint3(lower.x, lower_upper.y, upper.z))) * (1-factor.x)
              + (get_sdf(make_uint3(upper.x, upper_upper.y, upper.z)) - get_sdf(make_uint3(upper.x, lower_upper.y, upper.z))) * factor.x) * factor.y) * factor.z;

    gradient.z =
        (((get_sdf(make_uint3(lower.x, lower.y, upper_lower.z)) - get_sdf(make_uint3(lower.x, lower.y, lower_lower.z))) * (1-factor.x)
          + (get_sdf(make_uint3(upper.x, lower.y, upper_lower.z)) - get_sdf(make_uint3(upper.x, lower.y, lower_lower.z))) * factor.x) * (1-factor.y)
         + ((get_sdf(make_uint3(lower.x, upper.y, upper_lower.z)) - get_sdf(make_uint3(lower.x, upper.y, lower_lower.z))) * (1-factor.x)
            + (get_sdf(make_uint3(upper.x, upper.y, upper_lower.z)) - get_sdf(make_uint3(upper.x, upper.y, lower_lower.z))) * factor.x) * factor.y) * (1-factor.z)
        + (((get_sdf(make_uint3(lower.x, lower.y, upper_upper.z)) - get_sdf(make_uint3(lower.x, lower.y, lower_upper.z))) * (1-factor.x)
            + (get_sdf(make_uint3(upper.x, lower.y, upper_upper.z)) - get_sdf(make_uint3(upper.x, lower.y, lower_upper.z))) * factor.x) * (1-factor.y)
           + ((get_sdf(make_uint3(lower.x, upper.y, upper_upper.z)) - get_sdf(make_uint3(lower.x, upper.y, lower_upper.z))) * (1-factor.x)
              + (get_sdf(make_uint3(upper.x, upper.y, upper_upper.z)) - get_sdf(make_uint3(upper.x, upper.y, lower_upper.z))) * factor.x) * factor.y) * factor.z;

    return gradient * make_float3(dim.x/size.x, dim.y/size.y, dim.z/size.z) * (0.5f * 0.00003051944088f);
  }

  /**
   * @brief init. Allocate memory resources for TSDF volume
   * @param s         The volume size
   * @param d         The volume dimension
   */
  void init(uint3 s, float3 d){
    size = s;
    dim = d;
    cudaMalloc(&data, size.x * size.y * size.z * sizeof(short2));
    cudaMalloc(&color,size.x * size.y * size.z * sizeof(uchar3));
  }

  /**
   * @brief release. Release all the tsdf resources
   */
  void release(){
    cudaFree(data);
    cudaFree(color);
    data = NULL;
    color= NULL;
  }
};

/**
 * @brief initVolume. Initialize the TSDF volume in cuda kernel.
 * @param volume      TSDF volume
 * @param val         values to be initialized to
 */
__global__
void initVolume( Volume volume, const float2 val );

/**
 * @brief integrate.  Integrate TSDF volume in cuda kenerl.
 * @param vol         TSDF volume
 * @param depth       Floating point depth map
 * @param invTrack    Inverse of camera pose extrinsic
 * @param K           Camera intrinsics
 * @param mu          Distribution threshold mu
 * @param maxweight   Maximal weight in sdf weighting
 */
__global__
void integrate( Volume vol,
                const Image<float> depth,
                const Matrix4 invTrack,
                const Matrix4 K,
                const float mu,
                const float maxweight);

/**
 * @brief initVolumeWrap. Simple wrapper to test volume initialization
 * @param volume      TSDF volume
 * @param val         Values to be initialized to
 */
void initVolumeWrap( Volume volume, const float val );

/**
 * @brief setBoxWrap. Box wrapper around simple test kernel
 * @param volume
 * @param min_corner
 * @param max_corner
 * @param val
 */
void setBoxWrap( Volume volume,
                 const float3 min_corner,
                 const float3 max_corner,
                 const float val );

/**
 * @brief setSphereWrap. Sphere wrapper around simple test kernel
 * @param volume
 * @param center
 * @param radius
 * @param val
 */
void setSphereWrap( Volume volume,
                    const float3 center,
                    const float radius,
                    const float val );
