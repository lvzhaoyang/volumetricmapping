/* ----------------------------------------------------------------------------

* Volumetric Mapping Libraries in CUDA

* The implementation is based on:
*   https://github.com/GerhardR/kfusion
* Thanks to Gerhard.R. for raw implementation

* -------------------------------------------------------------------------- */

/**
  * @file   cudaFilter.cu
  * @brief  Define all the cuda filtering methods
  * @author Zhaoyang Lv
  * @date   Dec. 2014
  */

#include "cudaFilter.h"
#include "cudaMatrix.h"


/* ************************************************************************* */

__global__
void bilateralFilter( Image<float> out,
                      const Image<float> in,
                      const Image<float> gaussian,
                      const float e_d,
                      const int r ) {
  const uint2 pos = thr2pos2();

  if(in[pos] == 0){
    out[pos] = 0;
    return;
  }

  float sum = 0.0f;
  float t = 0.0f;
  const float center = in[pos];

  for(int i = -r; i <= r; ++i) {
    for(int j = -r; j <= r; ++j) {
      const float curPix = in[make_uint2(clamp(pos.x + i, 0u, in.size.x-1), clamp(pos.y + j, 0u, in.size.y-1))];
      if(curPix > 0){
        const float mod = sq(curPix - center);
        const float factor = gaussian[make_uint2(i + r, 0)] * gaussian[make_uint2(j + r, 0)] * __expf(-mod / (2 * e_d * e_d));
        t += factor * curPix;
        sum += factor;
      }
    }
  }
  out[pos] = t / sum;
}

/* ************************************************************************* */

__global__
void halfSampleRobust( Image<float> out,
                       const Image<float> in,
                       const float e_d,
                       const int r ) {
  const uint2 pixel = thr2pos2();
  const uint2 centerPixel = 2 * pixel;

  if(pixel.x >= out.size.x || pixel.y >= out.size.y )
    return;

  float sum = 0.0f;
  float t = 0.0f;
  const float center = in[centerPixel];
  for(int i = -r + 1; i <= r; ++i){
    for(int j = -r + 1; j <= r; ++j){
      float current = in[make_uint2(clamp(make_int2(centerPixel.x + j, centerPixel.y + i), make_int2(0), make_int2(in.size.x - 1, in.size.y - 1)))]; // TODO simplify this!
      if(fabsf(current - center) < e_d){
        sum += 1.0f;
        t += current;
      }
    }
  }
  out[pixel] = t / sum;
}

/* ************************************************************************* */

__global__
void generateGaussian( Image<float> out,
                       float delta,
                       int radius ) {
  int x = threadIdx.x - radius;
  out[make_uint2(threadIdx.x,0)] = __expf(-(x * x) / (2 * delta * delta));
}
