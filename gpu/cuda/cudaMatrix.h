/* ----------------------------------------------------------------------------

* Volumetric Mapping Libraries in CUDA

* The implementation is based on:
*   https://github.com/GerhardR/kfusion
* Thanks to Gerhard.R. for raw implementation

* -------------------------------------------------------------------------- */

/**
  * @file   cudaMatrix.h
  * @brief  Matrix methods in cuda
  * @author Zhaoyang Lv
  * @date   Dec. 2014
  */

#pragma once

#include <iostream>

#include <cuda_runtime.h>
#include <vector_functions.h>

#include <gtsam/geometry/Pose3.h>

#include "cutil_math.h"

/**
 * @brief sq. Compute square in cuda
 * @param x       Input value
 *
 * @return squared value
 */
__forceinline__ __device__ float
sq( const float x ){
  return x*x;
}

/**
 * @brief The Matrix4 struct. For homogeneous matrix in cuda code
 */
struct Matrix4{
  float4 data[4];
};

/**
 * @brief get_translation
 * @param A
 *
 * @return
 */
inline __host__ __device__
float3 get_translation(const Matrix4 A) {
  return make_float3(A.data[0].w, A.data[1].w, A.data[2].w);
}

/**
 * @brief set_translation
 * @param A
 * @param T
 *
 * @return
 */
inline __host__ __device__
void set_translation( Matrix4& A, const float3& T) {
  A.data[0].w = T.x; A.data[1].w = T.y; A.data[2].w = T.z;
}

/**
 * @brief operator*
 * @param M
 * @param v
 *
 * @return
 */
inline __host__ __device__
float4 operator*( const Matrix4 & M, const float4 & v) {
  return make_float4( dot(M.data[0], v), dot(M.data[1], v), dot(M.data[2], v), dot(M.data[3], v));
}

/**
 * @brief operator*
 * @param M
 * @param v
 *
 * @return
 */
inline __host__ __device__
float3 operator*( const Matrix4 & M, const float3 & v) {
  return make_float3( dot(make_float3(M.data[0]), v) + M.data[0].w,
      dot(make_float3(M.data[1]), v) + M.data[1].w,
      dot(make_float3(M.data[2]), v) + M.data[2].w);
}

/**
 * @brief rotate
 * @param M
 * @param v
 *
 * return
 */
inline __host__ __device__
float3 rotate( const Matrix4 & M, const float3 & v) {
  return make_float3( dot(make_float3(M.data[0]), v),
      dot(make_float3(M.data[1]), v),
      dot(make_float3(M.data[2]), v));
}

/**
 * @brief operator <<
 * @param out
 * @param m
 * @return
 */
std::ostream & operator<< ( std::ostream & out, const Matrix4 & m );

/**
 * @brief operator *
 * @param A
 * @param B
 * @return
 */
Matrix4 operator* ( const Matrix4 & A, const Matrix4 & B);

/**
 * @brief inverse
 * @param A
 * @return
 */
Matrix4 inverse( const Matrix4& A );

/**
 * @brief transpose
 * @param A
 * @return
 */
Matrix4 transpose( const Matrix4 &A );

/**
 * @brief checkEqual
 * @param A
 * @param B
 * @return
 */
bool checkEqual(Matrix4& A, Matrix4& B);

/**
 * @brief pose3ToMatrix4
 * @param p
 * @return
 */
Matrix4 pose3ToMatrix4( const gtsam::Pose3 & p);

/**
 * @brief matrix4ToPose3
 * @param poseMatrix4
 * @return
 */
gtsam::Pose3 matrix4ToPose3(const Matrix4& poseMatrix4);
