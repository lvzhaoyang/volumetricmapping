/* ----------------------------------------------------------------------------

* Volumetric Mapping Libraries in CUDA

* The implementation is based on:
*   https://github.com/GerhardR/kfusion
* Thanks to Gerhard.R. for raw implementation

* -------------------------------------------------------------------------- */

/**
  * @file   cudaRaycast.cu
  * @brief  Raycasting methods in cuda
  * @author Zhaoyang Lv
  * @date   Dec. 2014
  */

#include "cutil_math.h"

#include "cudaRaycast.h"

// the same as ICP
// should use a better way to make it
#define POINT_INVALID -2

/* ************************************************************************* */

__device__
float4 raycast( const Volume volume,
                const uint2 pos,
                const Matrix4 view,
                const float nearPlane,
                const float farPlane,
                const float step,
                const float largestep){
  const float3 origin = get_translation(view); // watch out here
  const float3 direction = rotate(view, make_float3(pos.x, pos.y, 1.f));

  // intersect ray with a box
  // http://www.siggraph.org/education/materials/HyperGraph/raytrace/rtinter3.htm
  // compute intersection of ray with all six bbox planes
  const float3 invR = make_float3(1.0f) / direction;
  const float3 tbot = -1 * invR * origin;
  const float3 ttop = invR * (volume.dim - origin);

  // re-order intersections to find smallest and largest on each axis
  const float3 tmin = fminf(ttop, tbot);
  const float3 tmax = fmaxf(ttop, tbot);

  // find the largest tmin and the smallest tmax
  const float largest_tmin = fmaxf(fmaxf(tmin.x, tmin.y), fmaxf(tmin.x, tmin.z));
  const float smallest_tmax = fminf(fminf(tmax.x, tmax.y), fminf(tmax.x, tmax.z));

  // check against near and far plane
  const float tnear = fmaxf(largest_tmin, nearPlane);
  const float tfar = fminf(smallest_tmax, farPlane);

  if(tnear < tfar) {
    // first walk with largesteps until we found a hit
    float t = tnear;
    float stepsize = largestep;
    float f_t = volume.interp_sdf(origin + direction * t);
    float f_tt = 0;
    if( f_t > 0){     // ups, if we were already in it, then don't render anything here
      for(; t < tfar; t += stepsize){
        f_tt = volume.interp_sdf(origin + direction * t);
        if(f_tt < 0)                               // got it, jump out of inner loop
          break;
        if(f_tt < 0.8f)                            // coming closer, reduce stepsize
          stepsize = step;
        f_t = f_tt;
      }
      if(f_tt < 0){                               // got it, calculate accurate intersection
        t = t + stepsize * f_tt / (f_t - f_tt);
        return make_float4(origin + direction * t, t);
      }
    }
  }
  return make_float4(0);
}

/* ************************************************************************* */

__global__
void raycast( Image<float3> pos3D,
              Image<float3> normal,
              const Volume volume,
              const Matrix4 view,
              const float nearPlane,
              const float farPlane,
              const float step,
              const float largestep) {
  const uint2 pos = thr2pos2();

  const float4 hit = raycast( volume, pos, view, nearPlane, farPlane, step, largestep );
  if(hit.w > 0){
    pos3D[pos] = make_float3(hit);
    float3 surfNorm = volume.grad(make_float3(hit));
    if(length(surfNorm) == 0){
      normal[pos].x = POINT_INVALID;
    } else {
      normal[pos] = normalize(surfNorm);
    }
  } else {
    pos3D[pos] = make_float3(0);
    normal[pos] = make_float3(POINT_INVALID, 0, 0);
  }
}

/* ************************************************************************* */

__global__
void raycastLight( Image<uchar4> render,
                   const Volume volume,
                   const Matrix4 view,
                   const float nearPlane,
                   const float farPlane,
                   const float step,
                   const float largestep,
                   const float3 light,
                   const float3 ambient ) {
  const uint2 pos = thr2pos2();

  float4 hit = raycast( volume, pos, view, nearPlane, farPlane, step, largestep);
  if(hit.w > 0){
    const float3 test = make_float3(hit);
    const float3 surfNorm = volume.grad(test);
    if(length(surfNorm) > 0){
      const float3 diff = normalize(light - test);
      const float dir = fmaxf(dot(normalize(surfNorm), diff), 0.f);
      const float3 col = clamp(make_float3(dir) + ambient, 0.f, 1.f) * 255;
      render.el() = make_uchar4(col.x, col.y, col.z,0);
    } else {
      render.el() = make_uchar4(0,0,0,0);
    }
  } else {
    render.el() = make_uchar4(0,0,0,0);
  }
}

/* ************************************************************************* */

__global__
void raycastInput( Image<float3> pos3D,
                   Image<float3> normal,
                   Image<float> depth,
                   const Volume volume,
                   const Matrix4 view,
                   const float nearPlane,
                   const float farPlane,
                   const float step,
                   const float largestep ) {
  const uint2 pos = thr2pos2();

  float4 hit = raycast( volume, pos, view, nearPlane, farPlane, step, largestep);
  if(hit.w > 0){
    pos3D[pos] = make_float3(hit);
    depth[pos] = hit.w;
    float3 surfNorm = volume.grad(make_float3(hit));
    if(length(surfNorm) == 0){
      normal[pos].x = -2;
    } else {
      normal[pos] = normalize(surfNorm);
    }
  } else {
    pos3D[pos] = make_float3(0);
    normal[pos] = make_float3(0);
    depth[pos] = 0;
  }
}
