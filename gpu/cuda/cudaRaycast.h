/* ----------------------------------------------------------------------------

* Volumetric Mapping Libraries in CUDA

* The implementation is based on:
*   https://github.com/GerhardR/kfusion
* Thanks to Gerhard.R. for raw implementation

* -------------------------------------------------------------------------- */

/**
  * @file   cudaRaycast.h
  * @brief  Raycasting methods in cuda
  * @author Zhaoyang Lv
  * @date   Dec. 2014
  */

#pragma once

#include "cudaMatrix.h"
#include "cudaImage.h"
#include "cudaTSDF.h"

/**
 * @brief raycast
 * @param pos3D
 * @param normal
 * @param volume
 * @param view
 * @param nearPlane
 * @param farPlane
 * @param step
 * @param largestep
 */
__global__
void raycast( Image<float3> pos3D,
              Image<float3> normal,
              const Volume volume,
              const Matrix4 view,
              const float nearPlane,
              const float farPlane,
              const float step,
              const float largestep );

/**
 * @brief raycastLight
 * @param render
 * @param volume
 * @param view
 * @param nearPlane
 * @param farPlane
 * @param step
 * @param largestep
 * @param light
 * @param ambient
 */
__global__
void raycastLight( Image<uchar4> render,
                   const Volume volume,
                   const Matrix4 view,
                   const float nearPlane,
                   const float farPlane,
                   const float step,
                   const float largestep,
                   const float3 light,
                   const float3 ambient);

/**
 * @brief raycastInput
 * @param pos3D
 * @param normal
 * @param depth
 * @param volume
 * @param view
 * @param nearPlane
 * @param farPlane
 * @param step
 * @param largestep
 */
__global__
void raycastInput( Image<float3> pos3D,
                   Image<float3> normal,
                   Image<float> depth,
                   const Volume volume,
                   const Matrix4 view,
                   const float nearPlane,
                   const float farPlane,
                   const float step,
                   const float largestep);
