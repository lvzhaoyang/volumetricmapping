/* ----------------------------------------------------------------------------

* Volumetric Mapping Libraries in CUDA

* The implementation is based on:
*   https://github.com/GerhardR/kfusion
* Thanks to Gerhard.R. for raw implementation

* -------------------------------------------------------------------------- */

/**
  * @file   cudaFilter.h
  * @brief  Declare all the cuda filtering methods
  * @author Zhaoyang Lv
  * @date   Dec. 2014
  */


#include "cudaImage.h"

/**
 * @brief bilateralFilter. Bilateral Filtering of images in cuda
 * @param out       The output image
 * @param in        The input image
 * @param gaussian  The gaussian weighted image
 * @param e_d       Smoothing parameter
 * @param r         Half window size
 */
__global__
void bilateralFilter( Image<float> out,
                      const Image<float> in,
                      const Image<float> gaussian,
                      float e_d,
                      int r);

/**
 * @brief halfSampleRobust. Filter and half sample image
 * @param out       The output image
 * @param in        The input image
 * @param e_d       Smoothing parameter
 * @param r         Half window size
 */
__global__
void halfSampleRobust( Image<float> out,
                       const Image<float> in,
                       const float e_d,
                       const int r);

/**
 * @brief generateGaussian. Generate gaussian map
 * @param out       The output gaussian map
 * @param delta     Standard variance
 * @param radius    Distribution radius
 */
__global__
void generateGaussian( Image<float> out,
                       float delta,
                       int radius);

