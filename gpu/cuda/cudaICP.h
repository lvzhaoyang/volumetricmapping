/* ----------------------------------------------------------------------------

* Volumetric Mapping Libraries in CUDA

* The implementation is based on:
*   https://github.com/GerhardR/kfusion
* Thanks to Gerhard.R. for raw implementation

* -------------------------------------------------------------------------- */

/**
  * @file   cudaICP.h
  * @brief  Declare Interative-Closest Points methods in cuda
  * @author Zhaoyang Lv
  * @date   Dec. 2014
  */

#pragma once

#include <cuda_runtime.h>

#include "cudaMatrix.h"
#include "cudaImage.h"

/**
 * @brief The TrackData struct
 */
struct TrackData {
  int result;
  float error;
  float J[6];
};

/**
 * @brief The solveAndUpdate struct
 *  Solve the SVD function
 */
struct solveAndUpdate {

  float *data;
  Matrix4* pose;
  int width;
  int height;

  /**
   * @brief solveAndUpdate Constructor
   */
  solveAndUpdate():
    data(NULL) {
  }

  /**
   * @brief solveAndUpdate Constructor
   * @param d   data
   * @param p   camera pose
   */
  solveAndUpdate( float* d, Matrix4& p):
    data(d), pose(&p), width(32), height(8) {
  }

  /**
   * @brief solve
   * @param delta
   * @return  SVD solution status
   */
  bool solve(gtsam::Pose3& delta);

  float* dataPtr() {
    return data;
  }

  float operator()( const int a, const int b ) {
    return data[a * width + b];
  }
};

/**
 * @brief track.
 * @param output
 * @param inVertex
 * @param refVertex
 * @param refNormal
 * @param Track
 * @param view
 * @param dist_threshold
 * @param normal_threshold
 */
__global__
void track( Image<TrackData> output,
            const Image<float3> inVertex,
            const Image<float3> inNormal,
            const Image<float3> refVertex,
            const Image<float3> refNormal,
            const Matrix4 Ttrack,
            const Matrix4 view,
            const float dist_threshold,
            const float normal_threshold );

/**
 * @brief reduce
 * @param out
 * @param J
 * @param size
 */
__global__
void reduce( float * out,
             const Image<TrackData> J,
             const uint2 size);

/**
 * @brief trackAndReduce
 * @param out
 * @param inVertex
 * @param refVertex
 * @param refNormal
 * @param Ttrack
 * @param view
 * @param dist_threshold
 * @param normal_threshold
 */
__global__
void trackAndReduce( float * out,
                     const Image<float3> inVertex,
                     const Image<float3> inNormal,
                     const Image<float3> refVertex,
                     const Image<float3> refNormal,
                     const Matrix4 Ttrack,
                     const Matrix4 view,
                     const float dist_threshold,
                     const float normal_threshold );

