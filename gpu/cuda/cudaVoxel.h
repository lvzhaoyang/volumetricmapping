#include <vector_types.h>

/**
 * @brief The Voxel struct
 *  Currently not used yet. But it will be strongly recommended to use this.
 *  We need to work on more factors for each voxel
 *  ToDo: templated for probablistic models
 */
struct Voxel {
  short sdf;
  short weight;
  char3 color;

  static const float sdf2double_;
  static const float sdf2short_;
  static float with_texture_;
};

static const float Voxel::sdf2double_ = 0.0003051944088f;
static const float Voxel::sdf2short_ = 32766.0f;
static float Voxel::with_texture_ = false;
